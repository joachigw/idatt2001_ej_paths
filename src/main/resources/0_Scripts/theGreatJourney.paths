::The Shire
Welcome to the peaceful land of The Shire. You are a brave adventurer tasked with a perilous journey to destroy the One Ring and save Middle-earth. Your adventure begins here.

You find yourself in the picturesque village of Hobbiton, surrounded by lush green fields and cozy hobbit holes. The time has come to embark on your epic quest.
[Start your journey](The Old Forest){healthAction(100), goldAction(50), inventoryAction(Sword)}

::The Old Forest
As you enter the eerie depths of the Old Forest, the towering trees seem to whisper ancient secrets. The path ahead is treacherous and filled with unseen dangers.

You must navigate through the dense foliage and avoid the clutches of malevolent spirits that dwell within. The journey promises both great peril and hidden treasures.
[Continue deeper into the forest](The Barrow-downs){healthAction(-20)}
[Turn back](The Shire)

::The Barrow-downs
You arrive at the haunted Barrow-downs, where ancient burial mounds loom in the mist. Shadows dance around you as the spirits of the dead awaken.

Beware of the wights that guard the treasures of the ancient kings. Your bravery will be tested in this treacherous place.
[Search for treasure](The Haunted Tomb){healthAction(-30)}
[Escape the Barrow-downs](The Old Forest){healthAction(-10)}

::The Haunted Tomb
Inside the haunted tomb, you discover a cache of valuable artifacts. However, the wights awaken, craving the warmth of life.
[Face the wights](The Wight Battle){healthAction(-50)}
[Flee the tomb](The Barrow-downs){}

::The Wight Battle
A fierce battle ensues as you confront the wights. The air is thick with malevolence, but you stand firm against the undead foes.

Draw your sword and fight for your life!
[Attack](Defeat the Wights){healthAction(-30)}
[Use magic](Magic Spell){healthAction(-20)}

::Defeat the Wights
With a final strike, you defeat the wights and reclaim the stolen treasure. The haunting atmosphere lifts as peace settles in the tomb.

You can now collect the treasures and decide your next move.
[Collect treasures](Treasure Collection){goldAction(100)}
[Leave the tomb](The Barrow-downs){}

::Treasure Collection
As you collect the treasures, you find a rare and powerful artifact. Your journey just became more promising.
[Continue exploring](The Barrow-downs){inventoryAction(Magic Amulet)}

::The Elven Forest
You venture into the mystical Elven Forest, a realm of enchantment and ethereal beauty. The air is filled with the soft melody of elven songs and the fragrance of blooming flowers.

You encounter a wise elven guardian who offers guidance for your quest.
[Seek the elven guardian's advice](Elven Wisdom){scoreAction(10)}
[Explore the forest on your own](The Enchanted Glade){}

::Elven Wisdom
The elven guardian imparts ancient wisdom upon you, revealing hidden knowledge about the Ring's power and the forces aligned against you.

Armed with this newfound insight, you continue your journey.
[Continue exploring](The Enchanted Glade){}

::The Enchanted Glade
In the heart of the Elven Forest, you discover a serene glade bathed in gentle sunlight. It is a place of tranquility and serenity.

As you rest, you hear a soft voice coming from a nearby waterfall. Curiosity piques your interest, and you follow the sound.

You find a hidden cave behind the waterfall, its entrance adorned with glowing crystals. The cave beckons you to explore its mysteries.
[Enter the cave](The Crystal Cavern){healthAction(-10)}
[Stay in the glade](Rest and Reflect){}

::The Crystal Cavern
Inside the cave, you are mesmerized by the breathtaking sight of shimmering crystals illuminating the chamber. Their radiant glow seems to hold ancient power.

As you approach a pedestal at the center of the cavern, a mystical voice speaks to you, revealing a crucial piece of information about the Ring's true nature.

Armed with this knowledge, you feel more prepared to face the challenges ahead.
[scoreAction(20)]
[Continue your journey](The Misty Mountains){}

::Rest and Reflect
You decide to stay in the peaceful glade, allowing yourself a moment of respite. The beauty of the surroundings and the tranquil atmosphere soothe your weary soul.

After taking the time to rest and reflect on your journey thus far, you feel rejuvenated and ready to continue your quest.
[healthAction(20)]
[Continue your journey](The Misty Mountains){}

::The Misty Mountains
You venture into the perilous Misty Mountains, a vast range that is both majestic and treacherous. Towering peaks reach up to touch the sky, while treacherous paths wind through deep valleys.

The biting cold and harsh conditions test your endurance, but hidden within the mountains lies a crucial passage to your destination.

You must navigate the treacherous terrain, overcome obstacles, and face formidable creatures that dwell in these unforgiving heights.
[Scale the treacherous peak](The Lonely Mountain){healthAction(-40)}
[Explore the hidden caves](The Goblin Tunnels){healthAction(-30)}

::The Lonely Mountain
Atop the treacherous peak of the Misty Mountains stands the formidable Lonely Mountain, home to the great dragon Smaug. Your goal is to reach the dragon's lair and confront the ancient evil.

The path is dangerous and fraught with peril, but the fate of Middle-earth depends on your courage and determination.
[Face the dragon](Dragon's Challenge){healthAction(-60)}
[Flee from the mountain](The Misty Mountains){}

::Dragon's Challenge
In a climactic battle against the fearsome dragon Smaug, you engage in a deadly struggle. The dragon's fiery breath and mighty claws test your mettle.

Summoning all your strength and skill, you strive to vanquish the ancient evil and bring an end to its reign of terror.
[Attack](Defeat Smaug){healthAction(-50)}
[Use a powerful artifact](Artifact's Might){healthAction(-40)}

::Defeat Smaug
With a final blow, you bring down the mighty dragon Smaug. The echoes of your victory reverberate through the Lonely Mountain.

You stand in the dragon's lair, surrounded by the remnants of its hoarded treasure. But your quest is not yet complete.
[Continue your journey](The Black Gate){goldAction(200)}

::Artifact's Might
Empowered by the ancient artifact, you unleash its tremendous power upon the dragon Smaug. The force of the attack is overwhelming, and the dragon falls before you.

With the ancient evil vanquished, you collect the dragon's hoard and marvel at the artifact's might.
[goldAction(200)]
[Continue your journey](The Black Gate){}

::The Black Gate
You approach the imposing Black Gate, the stronghold of the Dark Lord's forces. It is a place of darkness and despair, guarded by fierce orcs and menacing creatures.

Your heart races as you prepare to face the ultimate test of your courage and resolve. The fate of Middle-earth hangs in the balance.
[Confront the Dark Lord's forces](Battle at the Black Gate){healthAction(-70)}
[Seek an alternate route](The Secret Pass){healthAction(-30)}

::Battle at the Black Gate
In a desperate battle against the Dark Lord's forces, you fight valiantly to defend the cause of good. The clash of swords, the roar of war cries, and the stench of blood fill the air.

Every swing of your weapon and every spell you cast is a testament to your unwavering determination.
[Attack](Victory and Triumph){healthAction(-60)}
[Use a powerful spell](Spell of Light){healthAction(-50)}

::Victory and Triumph
Against all odds, you emerge victorious from the battle at the Black Gate. The forces of darkness retreat, their morale shattered by your unwavering resolve.

You stand as a beacon of hope, knowing that your actions have turned the tide in the war against evil.
[scoreAction(50)]
[Continue your journey](The Final Confrontation){}

::Spell of Light
With a powerful incantation, you unleash a blinding light that engulfs the Dark Lord's forces. The brilliance of your spell scatters the darkness and disorients your enemies.

In the chaos that follows, you seize the opportunity to strike decisive blows, turning the tide of the battle in your favor.
[Continue your journey](The Final Confrontation){scoreAction(40)}

::The Secret Pass
You discover a hidden passageway that bypasses the heavily guarded Black Gate. The secret route is perilous, but it offers a glimmer of hope for a swift and stealthy approach.

As you traverse the treacherous path, you must stay vigilant and avoid detection by the enemy.
[Stealthily navigate the pass](Successful Infiltration){healthAction(-40)}
[Fall into an ambush](Ambushed by Orcs){healthAction(-50)}

::Successful Infiltration
Your stealthy approach proves successful as you navigate the secret pass without raising any alarms. You move like a shadow, evading the enemy's watchful eyes.

Your cunning and skill enable you to arrive closer to your ultimate destination undetected.
[Continue your journey](The Final Confrontation){}

::Ambushed by Orcs
Suddenly, a horde of orcs emerges from the shadows, surrounding you. They launch a vicious surprise attack, overwhelming you with their sheer numbers.

You must fight for your life and find a way to turn the tide against the relentless onslaught.
[Defend yourself](Survive the Ambush){healthAction(-60)}
[Escape the ambush](The Secret Pass){healthAction(-30)}