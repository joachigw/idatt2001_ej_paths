package edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal;

import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import java.io.Serializable;

/**
 * Represents a goal to reach a certain amount of health.
 */
public class HealthGoal implements Goal, Serializable {

  private final int healthGoal;


  /**
   * Creates a new HealthGoal.
   *
   * @param healthGoal the amount of health to reach.
   */
  public HealthGoal(int healthGoal) {
    this.healthGoal = healthGoal;
  }


  /**
   * Checks if the goal is fulfilled.
   *
   * @param player the player to check.
   * @return true if the goal is fulfilled, false otherwise.
   */
  @Override
  public boolean isFulfilled(Player player) {
    return player.getHealth() >= healthGoal;
  }


  /**
   * Returns a string representation of the goal.
   *
   * @return a string representation of the goal.
   */
  @Override
  public String toString() {
    return "Health Goal: reach '" + healthGoal + "' health.";
  }
}
