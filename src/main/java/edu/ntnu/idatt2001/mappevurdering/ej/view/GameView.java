package edu.ntnu.idatt2001.mappevurdering.ej.view;

import edu.ntnu.idatt2001.mappevurdering.ej.controllers.GameController;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.FontObserver;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Game;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Link;
import edu.ntnu.idatt2001.mappevurdering.ej.utils.FontSettings;
import java.io.IOException;
import java.util.Objects;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Screen;
import javafx.stage.Stage;


/**
 * Class representing the GameView.
 * Responsible for displaying the game window.
 * Implements FontObserver to be able to implement the Observer pattern for font changes.
 *
 * @version 2021-05-22
 */
public class GameView implements FontObserver {

  private final Stage stage;
  private final GameController controller;
  private final FontSettings fontSettings = FontSettings.getInstance();

  private final StackPane root = new StackPane();
  private final GridPane gridBase = new GridPane();


  private Game game;
  private Label currentPassageTitle;
  private VBox playerStats;
  private ListView<Link> linkList;
  private Label currentHealth;
  private Label currentGold;
  private Label currentScore;
  private Text textContent;
  private VBox inventoryStats;
  private ListView<String> inventory;
  private Label statName;
  private Label name;
  private Label playerTitle;
  private TextFlow currentPassageContent;
  private Button logButton;
  private Button goalsButton;
  private Label inventoryTitle;
  private ScrollPane inventoryScroll;


  /**
   * Creates a new GameView.
   *
   * @param stage the stage to display the view on
   * @param controller the controller to handle the view
   */
  public GameView(Stage stage, GameController controller) {
    this.stage = stage;
    this.controller = controller;
    fontSettings.attach(this);
  }


  /**
   * Creates the GameView window.
   *
   * @param load the game to create a window for
   */
  public void setup(Game load) {

    game = load;

    root.setPrefHeight(Screen.getPrimary().getBounds().getHeight() * 0.8);
    root.setPrefWidth(Screen.getPrimary().getBounds().getWidth() * 0.8);
    root.setAlignment(Pos.CENTER);

    gridBase.prefHeightProperty().bind(root.heightProperty());
    gridBase.prefWidthProperty().bind(root.widthProperty());
    gridBase.setAlignment(Pos.CENTER);


    // Add a menu bar
    MenuBar menuBar = createMenuBar();
    StackPane.setAlignment(menuBar, Pos.TOP_LEFT);

    //first part of grid from top
    Button returnButton = new Button("<- Return to main menu");
    GridPane.setConstraints(returnButton,  0, 0,  1, 1, HPos.LEFT, VPos.CENTER);
    GridPane.setMargin(returnButton, new Insets(20));
    returnButton.setOnAction(e -> controller.returnButtonClicked());

    currentPassageTitle = new Label(game.getCurrentPassage().getTitle());
    GridPane.setConstraints(currentPassageTitle,  1, 0, 1, 1, HPos.CENTER, VPos.BOTTOM);
    GridPane.setMargin(currentPassageTitle, new Insets(10));

    logButton = new Button("LOG");
    goalsButton = new Button("GOALS");
    GridPane.setConstraints(logButton, 2, 0, 1, 1, HPos.RIGHT, VPos.CENTER);
    GridPane.setConstraints(goalsButton, 2, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
    GridPane.setMargin(logButton, new Insets(0,  10,  0, 0));
    GridPane.setMargin(goalsButton, new Insets(0,  10,  0,  0));
    logButton.setOnAction(e -> controller.logButtonClicked());
    goalsButton.setOnAction(e -> controller.goalsButtonClicked());

    gridBase.getChildren().addAll(returnButton, currentPassageTitle, logButton, goalsButton);

    //second part of grid from top
    playerStats();
    mainContent();
    inventoryStats();

    //third part of grid from top
    createChoiceArea();


    //Finishing the view
    root.getChildren().addAll(gridBase, menuBar);


    //CSS
    //Set css-ids for the nodes and add the stylesheet to this scene
    menuBar.setId("menu-bar");
    returnButton.setId("return-button");
    currentPassageTitle.setId("title");
    logButton.setId("log-button");
    goalsButton.setId("goals-button");
    gridBase.setId("layout");
    root.setId("anchor-pane");
    playerStats.setId("stats-area");
    inventoryStats.setId("stats-area");
    inventory.setId("inventory-list");
    textContent.setId("passage-content-text");
    currentPassageContent.setId("passage-content-container");
    inventoryScroll.setId("inventory-scroll-pane");
    menuBar.setStyle("-fx-text-fill: black");

    // Ensure that the current font settings are applied
    updateFont();

    // Set scene and show stage
    stage.setTitle("Paths - Game");
    Scene scene = new Scene(root);
    scene.getStylesheets().add(Objects.requireNonNull(getClass()
        .getResource("/css/game-scene.css")).toExternalForm());
    stage.setScene(scene);
    stage.show();
  }


  /**
   * Creates the player stats area of the view.
   */
  private void playerStats() {
    playerStats = new VBox();
    //GridPane.setMargin(playerStats, new Insets(30));
    playerStats.prefHeightProperty().bind(root.heightProperty().multiply(0.45));

    HBox centeredTitle = new HBox();
    playerTitle = new Label("PLAYER STATS");
    centeredTitle.getChildren().add(playerTitle);
    centeredTitle.setAlignment(Pos.CENTER_LEFT);
    centeredTitle.setPadding(new Insets(0, 0, 10, 0));

    GridPane playerGrid = new GridPane();
    playerGrid.setAlignment(Pos.CENTER_LEFT);
    playerGrid.prefWidthProperty().bind(playerStats.widthProperty().multiply(0.5));
    playerGrid.prefHeightProperty().bind(playerStats.heightProperty().multiply(0.5));


    //Name part of the player box
    statName = new Label("Name: ");
    GridPane.setConstraints(statName, 0, 0, 1, 1, HPos.LEFT, VPos.CENTER);
    GridPane.setMargin(statName, new Insets(0, 0, 20, 0));
    name = new Label(game.getPlayer().getName());
    GridPane.setConstraints(name, 1, 0, 1, 1, HPos.RIGHT, VPos.CENTER);
    GridPane.setMargin(name, new Insets(0, 0, 20, 25));
    playerGrid.getChildren().addAll(statName, name);



    //Health part of the player box
    ImageView statHealth = new ImageView("file:src/main/resources/pictures/ICONS/heartIcon.png");
    statHealth.setFitHeight(35);
    statHealth.setPreserveRatio(true);

    // Set a tooltip for the score icon
    Tooltip healthTooltip = new Tooltip("Health");
    healthTooltip.setFont(Font.font(20));
    Tooltip.install(statHealth, healthTooltip);

    GridPane.setConstraints(statHealth, 0, 1, 1, 1, HPos.LEFT, VPos.CENTER);
    GridPane.setMargin(statHealth, new Insets(0, 0, 20, 0));
    currentHealth = new Label(String.valueOf(game.getPlayer().getHealth()));
    GridPane.setConstraints(currentHealth, 1, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
    GridPane.setMargin(currentHealth, new Insets(0, 0, 20, 25));
    playerGrid.getChildren().addAll(statHealth, currentHealth);

    //Gold part of the player box
    ImageView statGold = new ImageView("file:src/main/resources/pictures/ICONS/goldIcon.png");
    statGold.setFitHeight(35);
    statGold.setPreserveRatio(true);

    // Set a tooltip for the gold icon
    Tooltip goldTooltip = new Tooltip("Gold");
    goldTooltip.setFont(Font.font(20));
    Tooltip.install(statGold, goldTooltip);

    GridPane.setConstraints(statGold, 0, 2, 1, 1, HPos.LEFT, VPos.CENTER);
    GridPane.setMargin(statGold, new Insets(0, 0, 20, 0));
    currentGold = new Label(String.valueOf(game.getPlayer().getGold()));
    GridPane.setConstraints(currentGold, 1, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
    GridPane.setMargin(currentGold, new Insets(0, 0, 20, 25));
    playerGrid.getChildren().addAll(statGold, currentGold);

    //Score part of the player box
    ImageView statScore = new ImageView("file:src/main/resources/pictures/ICONS/scoreIcon.png");
    statScore.setFitHeight(35);
    statScore.setPreserveRatio(true);

    // Set a tooltip for the score icon
    Tooltip scoreTooltip = new Tooltip("Score");
    scoreTooltip.setFont(Font.font(20));
    Tooltip.install(statScore, scoreTooltip);

    GridPane.setConstraints(statScore, 0, 3, 1, 1, HPos.LEFT, VPos.CENTER);
    GridPane.setMargin(statScore, new Insets(0, 0, 10, 0));
    currentScore = new Label(String.valueOf(game.getPlayer().getScore()));
    GridPane.setConstraints(currentScore, 1, 3, 1, 1, HPos.RIGHT, VPos.CENTER);
    GridPane.setMargin(currentScore, new Insets(0, 0, 10, 25));
    playerGrid.getChildren().addAll(statScore, currentScore);


    //Finishing and adding all parts to player box
    playerStats.getChildren().addAll(centeredTitle, playerGrid);
    playerStats.setAlignment(Pos.CENTER);

    GridPane.setConstraints(playerStats, 0, 2, 1, 1, HPos.LEFT, VPos.CENTER);
    GridPane.setMargin(playerStats, new Insets(0, 0, 0, 40));
    playerStats.prefWidthProperty().bind(gridBase.widthProperty().multiply(0.2));
    gridBase.getChildren().add(playerStats);

  }


  /**
   * Creates the content area of the view.
   */
  private void mainContent() {
    VBox contentMain = new VBox();
    contentMain.prefHeightProperty().bind(root.heightProperty().multiply(0.45));
    contentMain.prefWidthProperty().bind(root.widthProperty().multiply(0.45));
    contentMain.setAlignment(Pos.CENTER);

    textContent = new Text(game.getCurrentPassage().getContent());
    currentPassageContent = new TextFlow(textContent);
    HBox centeredText = new HBox();
    centeredText.getChildren().add(currentPassageContent);
    centeredText.setAlignment(Pos.CENTER);
    centeredText.prefWidthProperty().bind(root.widthProperty().multiply(0.45));

    contentMain.getChildren().add(centeredText);



    GridPane.setConstraints(contentMain, 1, 2, 1, 1, HPos.CENTER, VPos.CENTER);
    GridPane.setMargin(contentMain, new Insets(50, 30, 50, 30));
    gridBase.getChildren().add(contentMain);
  }


  /**
   * Creates the inventory area of the view.
   */
  private void inventoryStats() {
    inventoryStats = new VBox();
    inventoryStats.setPadding(new Insets(30));

    HBox title = new HBox();
    inventoryTitle = new Label("INVENTORY");
    title.getChildren().add(inventoryTitle);
    title.setAlignment(Pos.CENTER);
    title.setPadding(new Insets(0, 0, 10, 0));

    HBox centered = new HBox();
    centered.setAlignment(Pos.CENTER_RIGHT);
    inventory = new ListView<>();
    inventoryScroll = new ScrollPane(inventory);
    inventoryScroll.setMinWidth(200);

    inventory.prefWidthProperty().bind(inventoryStats.widthProperty().multiply(0.5));
    inventoryScroll.prefWidthProperty().bind(playerStats.widthProperty().multiply(0.5));
    inventoryScroll.prefHeightProperty().bind(playerStats.heightProperty().multiply(0.5));
    inventoryScroll.setFitToWidth(true);
    centered.getChildren().add(inventoryScroll);

    inventoryStats.getChildren().addAll(title, centered);
    inventoryStats.setAlignment(Pos.CENTER);
    inventoryStats.prefHeightProperty().bind(playerStats.heightProperty());
    inventoryStats.prefWidthProperty().bind(playerStats.widthProperty());


    GridPane.setConstraints(inventoryStats, 2, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
    GridPane.setMargin(inventoryStats, new Insets(0, 40, 0, 0));
    inventoryStats.prefWidthProperty().bind(gridBase.widthProperty().multiply(0.2));
    gridBase.getChildren().add(inventoryStats);
  }


  /**
   * Creates the choice area of the view.
   */
  private void createChoiceArea() {
    linkList = new ListView<>();
    linkList.prefHeightProperty().bind(gridBase.heightProperty().multiply(0.2));
    linkList.prefWidthProperty().bind(gridBase.widthProperty().multiply(0.6));
    linkList.setOnMouseClicked(e -> {
      try {
        controller.linkChosen();
      } catch (IOException | InterruptedException ex) {
        throw new RuntimeException(ex);
      }
    });
    HBox centered = new HBox(linkList);
    centered.setAlignment(Pos.CENTER);

    linkList.getItems().addAll(game.getCurrentPassage().getLinks());
    linkList.refresh();

    linkList.setCellFactory(param -> new ListCell<Link>() {
      @Override
      protected void updateItem(Link item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null || item.getText() == null) {
          setText(null);
          setStyle(null);
        } else {
          setText(item.getText());
          setStyle("-fx-cursor: hand;"
              + "-fx-font-weight: " + fontSettings.getFontWeightString() + ";"
              + "-fx-font-size: " + fontSettings.getFontSize() + ";"
              + "-fx-background-color: linear-gradient(to left, rgba(0, 0, 0, 0) 0%, "
              + "rgba(0, 0, 0, 0.6) 50%, rgba(0, 0, 0, 0) 100%), "
              + "linear-gradient(to right, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.6) 50%,"
              + " rgba(0, 0, 0, 0) 100%);"
          );
        }
      }
    });

    GridPane.setConstraints(centered, 1, 3, 1, 1, HPos.CENTER, VPos.CENTER);
    GridPane.setMargin(centered, new Insets(20));
    gridBase.getChildren().add(centered);
  }


  /**
   * Gets the health currently shown in the view.
   *
   * @return crntHealth
   */
  public Label getCurrentHealth() {
    return currentHealth;
  }


  /**
   * Gets the gold currently shown in the view.
   *
   * @return crntGold
   */
  public Label getCurrentGold() {
    return currentGold;
  }


  /**
   * Gets the score currently shown in the view.
   *
   * @return crntScore
   */
  public Label getCurrentScore() {
    return currentScore;
  }


  /**
   * Gets the inventory currently shown in the view.
   *
   * @return inventory
   */
  public ListView<String> getInventory() {
    return inventory;
  }


  /**
   * Gets the linkList currently shown in the view.
   *
   * @return linkList
   */
  public ListView<Link> getLinkList() {
    return linkList;
  }


  /**
   * Gets the current passage title currently shown in the view.
   *
   * @return currentPassageTitle
   */
  public Label getCurrentPassageTitle() {
    return currentPassageTitle;
  }


  /**
   * Gets the current passage content currently shown in the view.
   *
   * @return textContent
   */
  public Text getCrntTextContent() {
    return textContent;
  }


  /**
   * Creates the menubar.
   *
   * @return the menubar
   */
  private MenuBar createMenuBar() {

    //Create menubar and menu items
    MenuItem helpMenuItem = new MenuItem("Help");
    helpMenuItem.setOnAction(e -> controller.showHelp());

    Menu helpMenu = new Menu("Show help", null, helpMenuItem);

    return new MenuBar(helpMenu);
  }


  /**
   * Updates the view based on changes in the font size setting.
   */
  @Override
  public void updateFont() {
    String fontStyle = "-fx-font-size: " + fontSettings.getFontSize()
        + "px; -fx-font-weight: " + fontSettings.getFontWeightString();

    currentHealth.setStyle(fontStyle);
    currentGold.setStyle(fontStyle);
    currentScore.setStyle(fontStyle);
    playerTitle.setStyle(fontStyle);
    name.setStyle(fontStyle);
    statName.setStyle(fontStyle);
    currentPassageContent.setStyle(fontStyle);
    logButton.setStyle(fontStyle);
    goalsButton.setStyle(fontStyle);
    inventoryTitle.setStyle(fontStyle);
  }
}
