package edu.ntnu.idatt2001.mappevurdering.ej.view;

import edu.ntnu.idatt2001.mappevurdering.ej.controllers.LoadGameController;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.FontObserver;
import edu.ntnu.idatt2001.mappevurdering.ej.utils.FontSettings;
import java.io.File;
import java.util.Objects;
import java.util.Optional;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 * Class for the view that displays the load game menu.
 *
 * @version 2021-05-17
 *
 * @author joachimgw
 */
public class LoadGameView implements FontObserver {

  private final Stage stage;
  private final LoadGameController controller;
  private final FontSettings fontSettings = FontSettings.getInstance();

  // Nodes to be updates when font settings are changed
  private Label description;
  private Button browseLocalFiles;


  /**
   * Constructor for LoadGameView.
   *
   * @param stage the stage to display the view on
   * @param controller the controller for the view
   */
  public LoadGameView(Stage stage, LoadGameController controller) {
    this.stage = stage;
    this.controller = controller;
    fontSettings.attach(this);
  }


  /**
   * Sets up the view.
   * Creates the layout and displays it on the stage.
   */
  public void setup() {



    // Create return to main menu button
    Button returnButton = new Button("<- Return to main menu");
    returnButton.setOnMouseClicked(e -> controller.returnButtonClicked());


    // Create title, description and browse button
    description = new Label("Load a previously saved game from your"
        + "computer or from the locally saved files.");
    Label title = new Label("Load game");
    browseLocalFiles = new Button("Browse local files");
    browseLocalFiles.setOnAction(e -> controller.browseButtonClicked());

    // Create table view for displaying files
    TableView<File> filesTableView = getFilesTableView();



    // Create parent nodes and scene
    VBox content = new VBox(title, description, browseLocalFiles, filesTableView);
    StackPane stackPane = new StackPane(content, returnButton);


    // Set alignment of nodes
    StackPane.setAlignment(returnButton, Pos.TOP_LEFT);
    content.setAlignment(Pos.CENTER);
    stackPane.setAlignment(Pos.CENTER);

    //Set css-ids for the nodes and add the stylesheet to this scene
    returnButton.setId("return-button");
    title.setId("title");
    description.setId("description");
    browseLocalFiles.setId("browse-local-files");
    filesTableView.setId("files-table-view");
    content.setId("content-v-box");
    // Create MenuBar
    MenuBar menuBar = createMenuBar();
    VBox root = new VBox(menuBar, stackPane);
    Scene scene = new Scene(root);
    scene.getStylesheets().add(Objects.requireNonNull(getClass()
        .getResource("/css/load-game-scene.css")).toExternalForm());

    // Ensure that the current font settings are applied
    updateFont();

    // Set the scene and show the stage
    stage.setTitle("Paths - Load Game");
    stage.setScene(scene);
    stage.show();
  }


  /**
   * Creates a table view for displaying files.
   *
   * @return the table view
   */
  private TableView<File> getFilesTableView() {

    // Create a table to display the locally saved ".paths"-files
    TableView<File> filesTableView = new TableView<>();

    // Set the columns to fit the table width, and set placeholder text if no files are found
    filesTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    filesTableView.setPlaceholder(new Label("No locally saved files found"));

    // Create file name column
    TableColumn<File, String> fileName = new TableColumn<>("File name");
    fileName.setReorderable(false);
    fileName.setCellValueFactory(cellData -> {
      String fileNameWithExtension = cellData.getValue().getName();
      int extensionIndex = fileNameWithExtension.lastIndexOf(".");
      String fileNameWithoutExtension = fileNameWithExtension.substring(0, extensionIndex);
      return new SimpleStringProperty(fileNameWithoutExtension);
    });

    // Create open button column
    TableColumn<File, String> open = new TableColumn<>("");
    open.setReorderable(false);
    open.setSortable(false);
    open.setCellFactory(column -> new TableCell<>() {
          private final Button openButton = new Button("Open");

      {
        openButton.setOnAction(event -> {
          File file = getTableView().getItems().get(getIndex());
          controller.loadGame(file);

        });
      }

          // Display button only if the row has a file
          @Override
          protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
              setGraphic(null);
            } else {
              setGraphic(openButton);
            }
        }
      });

    // Create delete button column
    TableColumn<File, String> delete = new TableColumn<>("");
    delete.setReorderable(false);
    delete.setSortable(false);
    delete.setCellFactory(column -> new TableCell<>() {
        private final Button deleteButton = new Button("Delete");

        {
        // Set the delete button style
        deleteButton.setStyle("-fx-background-color: darkred; -fx-text-fill: white");

        // Delete the file if the user clicks and confirms the delete button click
        deleteButton.setOnAction(event -> {
          File file = getTableView().getItems().get(getIndex());
          Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
          confirmation.setHeaderText("Confirm Game Deletion");
          confirmation.setContentText("Are you sure you want to delete the game '"
              + file.getName() + "'?\n"
              + "This action cannot be undone.");

          // Add delete and cancel buttons
          ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
          ButtonType deleteButton = new ButtonType("Delete", ButtonBar.ButtonData.OK_DONE);
          confirmation.getButtonTypes().setAll(deleteButton, cancelButton);

          // Show the confirmation dialog and get the result
          Optional<ButtonType> result = confirmation.showAndWait();

          // Delete the file if the user clicked the delete button
          if (result.isPresent() && result.get().getText().equals("Delete")) {
            filesTableView.getItems().remove(file);
            controller.deleteGame(file);
          }

          // Refresh the table view to reflect the possible changes
          filesTableView.refresh();
        });
        }

          // Display button only if the row has a file
          @Override
          protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
              setGraphic(null);
            } else {
              setGraphic(deleteButton);
            }
        }
        });

    // Add the columns to the table
    filesTableView.getColumns().add(fileName);
    filesTableView.getColumns().add(open);
    filesTableView.getColumns().add(delete);

    // Add the files from the local files folder to the table
    File localFilesFolder = new File("src/main/resources/1_Games/");
    File[] localFiles = localFilesFolder.listFiles();
    assert localFiles != null;
    for (File file : localFiles) {
      if (file.isFile() && file.getName().endsWith(".game")) {
        filesTableView.getItems().add(file);
      }
    }

    return filesTableView;
  }


  /**
   * Creates the menu bar for the view.
   *
   * @return the menu bar
   */
  private MenuBar createMenuBar() {

    //Create menubar and menu items
    MenuItem helpMenuItem = new MenuItem("Help");
    helpMenuItem.setOnAction(e -> controller.showHelp());

    Menu helpMenu = new Menu("Show help", null, helpMenuItem);

    return new MenuBar(helpMenu);
  }


  /**
   * Updates the view based on changes in the font size setting.
   */
  @Override
  public void updateFont() {
    description.setStyle("-fx-font-size: " + fontSettings.getFontSize() + "px; -fx-font-weight: "
        + fontSettings.getFontWeightString());
    browseLocalFiles.setStyle("-fx-font-size: " + fontSettings.getFontSize()
        + "px; -fx-font-weight: "
        + fontSettings.getFontWeightString());
  }
}
