package edu.ntnu.idatt2001.mappevurdering.ej.interfaces;

/**
 * An interface for font observers.
 */
public interface FontObserver {

  /**
   * Updates the font.
   */
  void updateFont();
}
