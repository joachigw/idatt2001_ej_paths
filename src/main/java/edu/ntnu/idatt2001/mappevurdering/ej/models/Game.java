package edu.ntnu.idatt2001.mappevurdering.ej.models;

import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.Goal;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;


/**
 * Class representing a Game-object.
 * Responsible for keeping track of the player, story, goals, current passage and current link.
 * Also responsible for restarting the game.
 * Implements Serializable to be able to save the game.
 *
 * @version 2021-05-22
 */
public class Game implements Serializable {
  private final String title;
  private Player player;
  private Story story;
  private final List<Goal> goals;
  private Passage currentPassage;
  private Link currentLink;

  private final Player originalPlayer;
  private final Story originalStory;
  private final Passage startPassage;
  private final Link startLink;


  /**
   * Creates a new Game. Also saves a copy of the original player, story,
   * passage and link for use when restarting game.
   *
   * @param title the title of the game.
   *
   * @param player the player
   *
   * @param story the story
   *
   * @param goals the goals
   *
   * @param currentPassage the current passage
   *
   * @param currentLink the current link
   *
   */
  public Game(String title, Player player, Story story, List<Goal> goals,
              Passage currentPassage, Link currentLink) {
    this.title = title;
    this.player = player;
    this.story = story;
    this.goals = goals;
    this.currentPassage = currentPassage;
    this.currentLink = currentLink;

    //Restart Variables
    this.originalPlayer = new Player.Builder(player.getName())
              .gold(player
                      .getGold())
              .health(player
                      .getHealth())
              .score(player
                      .getScore())
              .build();
    this.originalStory = new Story(story.getTitle(), new HashMap<>(story.getPassages()),
        story.getScript());
    this.startPassage = currentPassage;
    this.startLink = currentLink;
  }


  /**
   * gets the title.
   *
   * @return the title.
   */
  public String getTitle() {
    return title;
  }

  /**
   * gets the player.
   *
   * @return the player
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * gets the story.
   *
   * @return the story
   */
  public Story getStory() {
    return story;
  }

  /**
   * gets the goals.
   *
   * @return the goals
   */
  public List<Goal> getGoals() {
    return goals;
  }

  /**
   * gets the current passage.
   *
   *@return the current passage
   */
  public Passage getCurrentPassage() {
    return currentPassage;
  }

  /**
   * sets the current passage.
   *
   * @param currentPassage the current passage.
   */
  public void setCurrentPassage(Passage currentPassage) {
    this.currentPassage = currentPassage;
  }

  /**
   * gets the current link.
   *
   * @return the current link.
   */
  public Link getCurrentLink() {
    return currentLink;
  }

  /**
   * sets the current link.
   *
   * @param currentLink the current link
   */
  public void setCurrentLink(Link currentLink) {
    this.currentLink = currentLink;
  }


  /**
   * Restarts the game. Sets all the variables to its original state
   */
  public void restartGame() {
    player = new Player.Builder(originalPlayer.getName())
              .gold(originalPlayer
                      .getGold())
              .health(originalPlayer
                      .getHealth())
              .score(originalPlayer
                      .getScore())
              .build();

    story = new Story(originalStory.getTitle(), new HashMap<>(originalStory.getPassages()),
        originalStory.getScript());

    currentPassage = new Passage(startPassage.getTitle(), startPassage.content,
        startPassage.getLinks());

    currentLink = new Link(startLink.getText(), startLink.getReference(), startLink.getActions());
  }

}