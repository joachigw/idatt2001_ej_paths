package edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal;

import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;


/**
 * Interface for a goal.
 */
public interface Goal {

  /**
   * Checks if the goal is fulfilled.
   *
   * @param player the player to check.
   * @return true if the goal is fulfilled, false otherwise.
   */
  boolean isFulfilled(Player player);
}
