package edu.ntnu.idatt2001.mappevurdering.ej.controllers;

import edu.ntnu.idatt2001.mappevurdering.ej.filehandling.ReadFile;
import edu.ntnu.idatt2001.mappevurdering.ej.filehandling.WriteFile;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.Action;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.Goal;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Game;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Link;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Passage;
import edu.ntnu.idatt2001.mappevurdering.ej.view.GameView;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import javafx.stage.Stage;


/**
 * Class representing a Game Controller.
 * Responsible for handling the interactions between the view and the model.
 * Also responsible for handling the popup windows.
 *
 * @version 2021-05-22
 */
public class GameController {

  private final Stage stage;
  private final TitleScreenController titleScreenController;
  private GameView view;
  private Game game;

  private final ReadFile reader = new ReadFile();
  private final WriteFile writer = new WriteFile();
  private Popup popup;
  private HBox centeredTitle = new HBox();
  private final Button closeBtn = new Button("Close");
  Button retryBtn = new Button("Start from Scratch");


  /**
   * Constructor for LoadGameController.
   *
   * @param stage the stage to display the view on
   */
  public GameController(Stage stage, TitleScreenController titleScreenController) {
    this.stage = stage;
    this.titleScreenController = titleScreenController;
  }

  /**
   * Initializes the view.
   *
   * @see GameView
   */
  public void initialize(Game game) {
    centeredTitle.setAlignment(Pos.CENTER);
    closeBtn.setOnAction(e -> popup.hide());
    retryBtn.setOnAction(e -> {
      restartGame();
      popup.hide();
    });
    this.game = game;
    view = new GameView(stage, this);
    view.setup(game);
  }

  /**
   * Displays the game view.

   * @param game the game to display
   */
  private void displayGame(File game) {

  }

  /**
   * Returns to the title screen.
   */
  public void returnButtonClicked() {
    titleScreenController.initialize();
  }


  /**
   * Opens a file explorer window and returns the chosen file.
   * Verifies that the chosen file is a .paths file.
   */
  public void logButtonClicked() {
    TextArea log = new TextArea();
    log.setEditable(false);
    log.setWrapText(true);

    log.setText(logSorter());

    ScrollPane scroll = new ScrollPane(log);
    scroll.setFitToWidth(true);
    scroll.setPadding(new Insets(10));

    popup = new Popup();
    popup.setAutoHide(true);

    centeredTitle = new HBox(new Label("Your Journey Log"));

    popupButtons(scroll, closeBtn, retryBtn);
  }


  /**
   * Creates the interactions for the popup.
   *
   * @param scroll the scrollpane to be added to the popup
   * @param closeBtn the first button
   * @param retryBtn the second button
   */
  private void popupButtons(ScrollPane scroll, Button closeBtn, Button retryBtn) {
    HBox centeredBtn = new HBox(closeBtn, retryBtn);
    centeredBtn.setAlignment(Pos.CENTER);

    VBox box = new VBox(centeredTitle, scroll, centeredBtn);
    box.setStyle("-fx-background-color: white; -fx-border-color: black; -fx-border-width: 1px;");

    popup.getContent().addAll(box);
    popup.show(stage);
  }


  /**
   * Method that gets the opening passage of the game and sorts
   * the rest of the passages in the order of passages visited.
   *
   * @return a string with the sorted passages
   */
  public String logSorter() {
    try {
      Passage opPassage = reader.readPassage("First Passage", game.getStory().getScript());
      Link opLink = new Link("Opening Link", opPassage.getTitle(), null);
      StringBuilder logList = new StringBuilder();
      Integer logCount = 1;
      Map<Link, Passage> sortedLog = new HashMap<>(game.getStory().getPassages());
      Passage logPassage = game.getStory().getPassages().get(opLink);
      Link logPassageLink = logPassage.getLinks().get(0);
      logList.append(logPassage.getContent()).append("\n\n");
      sortedLog.remove(opLink);
      while (!(sortedLog.size() < 1)) {
        logPassage = sortedLog.get(logPassageLink);
        logList.append(logCount)
        .append(".\n")
        .append(logPassage.getContent())
            .append("\n\n");
        sortedLog.remove(logPassageLink);
        for (Map.Entry<Link, Passage> entry : sortedLog.entrySet()) {
          if (logPassage.getLinks().contains(entry.getKey())) {
            logPassageLink = entry.getKey();
          }
        }
        logCount += 1;
      }
      return logList.toString();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }


  /**
   * Method that gathers the link chosen by the user from the game view.
   * If the link is a "End Story" link, the game is removed from the list of games
   * and the user is returned to the title screen.
   * If the link is a "Start Over" link, the game is restarted.
   * If the link is a "Broken Link" link, a popup is displayed to the user.
   * If the link is a "Normal Link", the passage associated with the link is displayed.
   *
   * @throws IOException if the file cannot be read
   * @throws InterruptedException if the thread is interrupted
   */
  public void linkChosen() throws IOException, InterruptedException {
    ListView<Link> list = view.getLinkList();
    Link chosenLink = list.getSelectionModel().getSelectedItem();

    if (chosenLink == null) {
      return;
    }

    if (chosenLink.getText().contains("End Story")) {
      writer.removeGame(game);
      returnButtonClicked();
      return;
    }
    if (chosenLink.getText().contains("Start Over")) {
      restartGame();
      initialize(game);
      return;
    }
    if ((reader.readPassage(chosenLink.getReference(), game.getStory().getScript()) == null)) {
      popup = new Popup();
      centeredTitle = new HBox(new Text("Broken Link"));
      TextArea area = new TextArea(("End of Link. This link lead nowhere"));
      ScrollPane scroll = new ScrollPane(area);
      popupButtons(scroll, closeBtn, retryBtn);
      returnButtonClicked();
      return;
    }

    Link returningLink = null;
    Passage newPassage = reader.readPassage(chosenLink.getReference(), game.getStory().getScript());
    if (game.getStory().getPassages().containsValue(newPassage)) {
      list.getItems().removeAll(game.getCurrentPassage().getLinks());
      game.getStory().getPassages().remove(game.getCurrentLink(), game.getCurrentPassage());
      for (Map.Entry<Link, Passage> entry : game.getStory().getPassages().entrySet()) {
        if (entry.getKey().getReference().equals(chosenLink.getReference())) {
          returningLink = entry.getKey();
        }
      }
      game.setCurrentLink(returningLink);
      game.setCurrentPassage(game.getStory().getPassages().get(returningLink));
      view.getCrntTextContent().setText("""
          You return to the divided path.
          What do you choose now?

          """);


    } else {
      list.getItems().removeAll(game.getCurrentPassage().getLinks());
      game.setCurrentLink(chosenLink);
      game.setCurrentPassage(newPassage);

      StringBuilder activities = new StringBuilder();
      if (game.getCurrentLink().getActions().size() > 0) {
        activities.append("Actions:\n\n");
        for (Action action : game.getCurrentLink().getActions()) {
          action.execute(game.getPlayer());
          activities.append(" - ").append(action).append("\n");
        }



        TextArea thisHappened = new TextArea(activities.toString());
        thisHappened.setEditable(false);
        ScrollPane scroll = new ScrollPane(thisHappened);
        scroll.setFitToWidth(true);

        popup = new Popup();
        popup.setAutoHide(true);

        centeredTitle = new HBox(new Label("This has happened"));
        centeredTitle.setAlignment(Pos.CENTER);

        HBox centeredBtn = new HBox(closeBtn);
        centeredBtn.setAlignment(Pos.CENTER);

        VBox box = new VBox(centeredTitle, scroll, centeredBtn);
        box.setStyle("-fx-background-color: white; -fx-border-color: black; "
            + "-fx-border-width: 1px;");

        popup.getContent().addAll(box);
      }

      view.getCurrentHealth().setText(String.valueOf(game.getPlayer().getHealth()));
      view.getCurrentGold().setText(String.valueOf(game.getPlayer().getGold()));
      view.getCurrentScore().setText(String.valueOf(game.getPlayer().getScore()));
      view.getInventory().getItems().clear();
      for (String item : game.getPlayer().getInventory()) {
        view.getInventory().getItems().add(item);
      }


      if (checkGoals()) {
        popup = new Popup();
        popup.setAutoHide(false);

        TextArea goalReached = new TextArea(reachedGoal());
        goalReached.setEditable(false);
        ScrollPane scroll = new ScrollPane(goalReached);
        scroll.setFitToWidth(true);

        closeBtn.setText("Exit (Game Deletes)");
        closeBtn.setOnAction(e -> {
          Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
          confirmation.setHeaderText("Confirm Game Deletion");
          confirmation.setContentText("Are you sure you want to delete the game '"
              + game.getTitle() + "'?\n"
              + "This action cannot be undone.");

          // Add delete and cancel buttons
          ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
          ButtonType deleteButton = new ButtonType("Delete", ButtonBar.ButtonData.OK_DONE);
          confirmation.getButtonTypes().setAll(deleteButton, cancelButton);

          // Show the confirmation dialog and get the result
          Optional<ButtonType> result = confirmation.showAndWait();

          // Delete the file if the user confirms the deletion
          if (result.isPresent() && result.get().getText().equals("Delete")) {
            writer.removeGame(game);
            returnButtonClicked();
          }

          popup.hide();
        });


        popupButtons(scroll, retryBtn, closeBtn);
      } else if (game.getCurrentLink().getActions().size() > 0) {
        popup.show(stage);
      }
      view.getCrntTextContent().setText(game.getCurrentPassage().getContent());
    }

    game.getStory().getPassages().put(game.getCurrentLink(), game.getCurrentPassage());
    view.getLinkList().getItems().addAll(game.getCurrentPassage().getLinks());
    view.getCurrentPassageTitle().setText(game.getCurrentPassage().getTitle());
    writer.saveGame(game);

    list.refresh();

  }


  /**
   * Returns a string with the goal that was fulfilled if one is fulfilled.
   *
   * @return a string with the goal that was fulfilled.
   */
  private String reachedGoal() {
    for (Goal goal : game.getGoals()) {
      if (goal.isFulfilled(game.getPlayer())) {
        return "You fulfilled the " + goal.getClass().getSimpleName() + "!\n - " + goal;
      }
    }
    if (game.getPlayer().getHealth() == 0) {
      centeredTitle = new HBox(new Label("You Died...."));
      centeredTitle.setAlignment(Pos.CENTER);
      return "Unfortunately your health reached zero. This means Game Over";
    }

    return null;
  }

  /**
   * Checks if any of the goals are fulfilled.
   *
   * @return true if any of the goals are fulfilled
   */
  private boolean checkGoals() {
    for (Goal goal : game.getGoals()) {
      if (goal.isFulfilled(game.getPlayer())) {
        return true;
      }
    }
    return game.getPlayer().getHealth() == 0;
  }


  /**
   * Restarts the game.
   */
  private void restartGame() {
    game.restartGame();
    initialize(game);
    popup.hide();
  }


  /**
   * Shows the help dialog for the options screen.
   */
  public void showHelp() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle("Game Help");
    alert.setHeaderText(null);
    alert.setContentText("""
        Game View Help
                
        Welcome to the Game View!\s
        This is where you can play the game and interact with its elements.\s
        Here's a guide to help you understand the features and how to use them:
                 
                
        Player Stats:
                
        In the "PLAYER STATS" section, you can see the details of the main player character.
        The character's name is displayed under the "Name" label.
        You can view the character's current health, gold, and score in their respective fields.
        
        
        Inventory:
                
        In the "INVENTORY" section, you can manage the character's inventory.
        The list of items in the inventory is displayed.
        To add an item to the inventory, click the "Add item" button and select items from the list.
        To remove an item from the inventory, right-click on the item and choose "Remove."
        
        
        Content Area:
                
        The main content area displays the current passage of the game.
        You can read the passage text in the centered text box.
        
        
        Choices:
                
        In the choice area (below the content area  in the middle), \s
        you can see the available choices for the current passage.
        The choices are displayed as a list.
        Click on a choice to proceed to the corresponding passage.
        
        
        Navigation:
                
        To return to the main menu, click the "<- Return to main menu" button.
        To view the game log, click the "LOG" button.
               
                
        That's it! Now you know how to navigate and interact with the Game View \s
        to play the game and enjoy the interactive story. Have fun playing!""");

    alert.getDialogPane().setPrefWidth(900);
    alert.showAndWait();
  }


  /**
   * Shows the game's goals.
   */
  public void goalsButtonClicked() {
    ListView<Goal> goals = new ListView<>();
    goals.setEditable(false);
    goals.setCellFactory(param -> new ListCell<>() {
      @Override
      protected void updateItem(Goal item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null || item.toString() == null) {
          setText(null);
        } else {
          setText(item.toString());
          setStyle("-fx-text-fill: black");
        }
      }
    });

    for (Goal goal : game.getGoals()) {
      goals.getItems().add(goal);
    }

    ScrollPane scroll = new ScrollPane(goals);
    scroll.setFitToWidth(true);
    scroll.setPadding(new Insets(10));

    popup = new Popup();
    popup.setAutoHide(true);

    centeredTitle = new HBox(new Label("Your Goals"));

    HBox centeredBtn = new HBox(closeBtn);
    centeredBtn.setAlignment(Pos.CENTER);
    centeredBtn.setStyle("-fx-padding: 10px");

    VBox box = new VBox(centeredTitle, scroll, centeredBtn);
    box.setStyle("-fx-background-color: white; -fx-border-color: black;"
        + " -fx-border-width: 1px; -fx-text-fill: black");

    popup.getContent().addAll(box);
    popup.show(stage);
  }
}
