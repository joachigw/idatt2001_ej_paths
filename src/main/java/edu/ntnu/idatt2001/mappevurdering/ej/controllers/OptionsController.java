package edu.ntnu.idatt2001.mappevurdering.ej.controllers;

import edu.ntnu.idatt2001.mappevurdering.ej.utils.FontSettings;
import edu.ntnu.idatt2001.mappevurdering.ej.view.OptionsView;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

/**
 * Controller class for the options screen.
 *
 * @version 2021-05-18
 * @author joachimgw
 */
public class OptionsController {

  private final Stage stage;
  private final TitleScreenController titleScreenController;
  private final FontSettings fontSettings;


  /**
   * Constructor for OptionsController.
   *
   * @param stage the stage to display the view on
   * @param titleScreenController the title screen controller
   */
  public OptionsController(Stage stage, TitleScreenController titleScreenController) {
    this.stage = stage;
    this.titleScreenController = titleScreenController;
    this.fontSettings = FontSettings.getInstance();
  }


  /**
   * Initializes the view.
   *
   * @see OptionsView for more information.
   */
  public void initialize() {
    OptionsView view = new OptionsView(stage, this);
    view.setup();
  }


  /**
   * Returns to the title screen.
   */
  public void returnButtonClicked() {
    titleScreenController.initialize();
  }


  /**
   * Shows the help dialog for the options screen.
   */
  public void showHelp() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle("Options Help");
    alert.setHeaderText(null);
    alert.setContentText("""
            Options Help

            This page allows you to customize various settings for the application.

            Font Size:
               - Choose the desired font size for the application.
               - Available options: Small, Medium, Large.

            Bold Font:
               - Enable or disable bold font for the application.

            Use the choice box and checkbox to adjust the settings.

            To return to the main menu, click the '<- Return to main menu' button.""");

    alert.showAndWait();
  }
}
