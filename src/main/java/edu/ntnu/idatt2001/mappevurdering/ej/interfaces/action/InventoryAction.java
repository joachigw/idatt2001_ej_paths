package edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action;

import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import java.io.Serializable;


/**
 * Represents an action that adds an item to the player's inventory.
 */
public class InventoryAction implements Action, Serializable {

  private final String inventoryItem;

  /**
   * Creates a new InventoryAction.
   *
   * @param inventoryItem the item to add to the inventory.
   */
  public InventoryAction(String inventoryItem) {
    this.inventoryItem = inventoryItem;
  }

  /**
   * Executes the action.
   *
   * @param player the player to execute the action on.
   */
  @Override
  public void execute(Player player) {
    player.addToInventory(this.inventoryItem);
  }


  /**
   * Returns a string representation of the action.
   *
   * @return a string representation of the action.
   */
  @Override
  public String toString() {
    return "You got a new item: '" + this.inventoryItem + "' acquired!";
  }

  /**
   * Returns a string representation of the action.
   *
   * @return a string representation of the action.
   */
  public String toFileString() {
    return "InventoryAction(" + this.inventoryItem + ")";
  }

}
