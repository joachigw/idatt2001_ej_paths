package edu.ntnu.idatt2001.mappevurdering.ej.filehandling;

import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.Action;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.GoldAction;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.HealthAction;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.InventoryAction;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.ScoreAction;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Game;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Link;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Passage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Represents a file reader.
 */
public class ReadFile {

  /**
   * Reads a game from a file.
   *
   * @param file the file to read from
   * @return the game
   */
  public Game readGame(File file) {
    try {
      ObjectInputStream load = new ObjectInputStream(new FileInputStream(file));
      Game foundGame = (Game) load.readObject();
      load.close();
      return foundGame;
    } catch (IOException | ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Reads a passage from a file.
   *
   * @param chosen the chosen passage
   * @param script the script to read from
   * @return the passage
   * @throws IOException if the file is not found
   */
  public Passage readPassage(String chosen, File script) throws IOException {
    try {
      Passage searchedPassage = null;
      if (chosen.equals("First Passage")) {
        try (Scanner in = new Scanner(script)) {
          while (in.hasNextLine() && chosen.equals("First Passage")) {
            String currentLine = in.nextLine().trim();
            if (!currentLine.isEmpty()) {
              chosen = currentLine.substring(2).trim();
            }
          }
        }
      }
      try (Scanner in = new Scanner(script)) {
        while (in.hasNextLine()) {
          String currentLine = in.nextLine().trim();
          if (currentLine.startsWith("::") && currentLine.contains(chosen)) {
            String passageTitle = currentLine.substring(2).trim();
            StringBuilder passageContent = new StringBuilder();
            currentLine = in.nextLine().trim();
            while (!currentLine.startsWith("[")) {
              passageContent.append(currentLine).append("\n");
              if (in.hasNextLine()) {
                currentLine = in.nextLine().trim();
              } else {
                break;
              }
            }
            ArrayList<Link> passageLinkList = new ArrayList<>();
            while (currentLine.startsWith("[")) {
              passageLinkList.add(formatPassageLink(currentLine));
              if (in.hasNextLine()) {
                currentLine = in.nextLine().trim();
              } else {
                break;
              }
            }
            searchedPassage = new Passage(passageTitle, passageContent.toString(), passageLinkList);
          }
        }
      }
      return searchedPassage;
    } catch (NumberFormatException e) {
      throw new NumberFormatException("Fault in the .paths file: " + e);
    } catch (IOException e) {
      throw new IOException("Fault in reading the passage");
    }
  }

  /**
   * Formats a passage link to add it correctly from the file.
   *
   * @param currentLine the current line to format
   *
   * @return the formatted link
   */
  private Link formatPassageLink(String currentLine) {
    String linkText = null;
    if (currentLine.contains("[")) {
      linkText = currentLine.substring(currentLine.indexOf("[") + 1, currentLine.indexOf("]"));
    }
    String linkReference = null;
    if (currentLine.contains("(")) {
      linkReference = currentLine.substring(currentLine.indexOf("(") + 1, currentLine.indexOf(")"));
    }
    String[] actionData;
    ArrayList<Action> actionList = new ArrayList<>();
    if (currentLine.contains("{") && currentLine.toLowerCase().contains("action")) {
      actionData = (currentLine.substring(currentLine.indexOf("{") + 1,
          currentLine.indexOf("}"))).split(",");
      for (String line : actionData) {
        String actionName = line.split("\\(")[0].toLowerCase().replace(" ", "");
        if (actionName.equals("inventoryaction")) {
          String item = line.substring(line.indexOf("(") + 1, line.indexOf(")"));
          actionList.add(new InventoryAction(item));
        } else if (actionName.contains("action")) {
          int value = Integer.parseInt(line.replaceAll("[^0-9\\-]", ""));
          actionList.add(createAction(actionName, value));
        }
      }
    }
    return new Link(linkText, linkReference, actionList);
  }

  /**
   * Creates an action.
   *
   * @param actionName the name of the action
   * @param value the value of the action
   * @return the action
   */
  private Action createAction(String actionName, int value) {
    return switch (actionName) {
      case "goldaction" -> new GoldAction(value);
      case "healthaction" -> new HealthAction(value);
      case "scoreaction" -> new ScoreAction(value);
      default -> null;
    };
  }

  /**
   * Gets all the broken links in a file.
   *
   * @param script the script to read from
   *
   * @return the broken links
   */
  public String getBrokenLinks(File script) {

    HashSet<Passage> allPassage = new HashSet<>();
    StringBuilder brokenLinks = new StringBuilder();
    brokenLinks.append("\n\nThere are broken links:\n");

    try (Scanner in = new Scanner(script)) {
      while (in.hasNextLine()) {
        String currentLine = in.nextLine().trim();
        if (currentLine.startsWith("::")) {
          allPassage.add(readPassage(currentLine.substring(2).trim(), script));
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    Set<String> passageTitles = allPassage.stream()
                                            .map(Passage::getTitle)
                                            .collect(Collectors.toSet());


    for (Passage passage : allPassage) {
      for (Link links : passage.getLinks()) {
        if (!passageTitles.contains(links.getReference())
                    & (links.getReference() == null
                    || (!links.getReference().equals("Previous Passage")
                    && !links.getReference().equals("End Story")))) {
          if (!brokenLinks.toString().contains(passage.getTitle())) {
            brokenLinks.append("\nPassage: ")
                    .append(passage.getTitle())
                    .append("\n");
          }
          brokenLinks.append(" - ")
                    .append(links.toSmallString())
                    .append("\n");
        }
      }
    }
    if (brokenLinks.length() < 30) {
      brokenLinks.delete(0, brokenLinks.length());
      brokenLinks.append("There are no broken links");
    }

    return brokenLinks.toString();
  }
}
