package edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action;

import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;

/**
 * Interface for an action.
 */
public interface Action {

  /**
   * Executes the action.
   *
   * @param player the player to execute the action on.
   */
  void execute(Player player);


  /**
   * Returns a string representation of the action.
   *
   * @return a string representation of the action.
   */
  String toFileString();
}

