package edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action;

import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import java.io.Serializable;


/**
 * Represents an action that adds or removes the player's gold amount.
 */
public class GoldAction implements Action, Serializable {

  private final int gold;


  /**
   * Creates a new GoldAction.
   *
   * @param gold the amount of gold to add.
   */
  public GoldAction(int gold) {
    this.gold = gold;
  }


  /**
   * Executes the action.
   *
   * @param player the player to execute the action on.
   */
  @Override
  public void execute(Player player) {
    player.addGold(this.gold);
  }


  /**
   * Returns a string representation of the action.
   *
   * @return a string representation of the action.
   */
  @Override
  public String toString() {
    if (this.gold < 0) {
      String negative = String.valueOf(this.gold);
      return "You lost: " + negative.replace("-", "") + " gold...";
    } else {
      return "You gained: " + this.gold + " gold!";
    }
  }


  /**
   * Returns a string representation of the action.
   *
   * @return a string representation of the action.
   */
  public String toFileString() {
    return "GoldAction(" + this.gold + ")";
  }
}