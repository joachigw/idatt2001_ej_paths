package edu.ntnu.idatt2001.mappevurdering.ej.view;

import edu.ntnu.idatt2001.mappevurdering.ej.controllers.TitleScreenController;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.FontObserver;
import edu.ntnu.idatt2001.mappevurdering.ej.utils.FontSettings;
import java.util.Objects;
import javafx.animation.FadeTransition;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;


/**
 * View class for the title screen.
 *
 * @author joachimgw
 */
public class TitleScreenView implements FontObserver {

  private final Stage stage;
  private final TitleScreenController controller;
  private final FontSettings fontSettings;

  // Nodes to be updates when font settings are changed
  private Button newGame;
  private Button loadGame;
  private Button options;
  private Button quit;


  /**
   * Constructor for the TitleScreenView2 class.
   *
   * @param stage the stage to display the view on
   * @param controller the controller for the view
   */
  public TitleScreenView(Stage stage, TitleScreenController controller) {
    this.stage = stage;
    this.controller = controller;
    this.fontSettings = FontSettings.getInstance();
    fontSettings.attach(this);
  }


  /**
   * Sets up the view.
   * Creates the layout and displays it on the stage.
   */
  public void setup() {





    // Create menu buttons
    newGame = new Button("New game");
    loadGame = new Button("Load game");
    options = new Button("Options");
    quit = new Button("Quit");

    // Event handlers for the menu buttons
    newGame.setOnAction(e -> controller.newGame());
    loadGame.setOnAction(e -> controller.loadGame());
    options.setOnAction(e -> controller.openOptions());
    quit.setOnAction(e -> controller.exit());

    // Add fade transitions to the menu buttons
    Button[] buttons = {newGame, loadGame, options, quit};
    for (Button button : buttons) {
      // Create a fade transition for the color change
      FadeTransition fadeTransition = new FadeTransition(Duration.seconds(0.6), button);
      fadeTransition.setFromValue(0);
      fadeTransition.setToValue(1);

      // Set button style on mouse enter
      button.setOnMouseEntered(e -> {
        fadeTransition.playFromStart();
        button.setStyle("-fx-background-color: linear-gradient(to left, rgba(0, 0, 0, 0) 0%, "
            + "rgba(0, 0, 0, 0.6) 50%, rgba(0, 0, 0, 0) 100%), "
            + "linear-gradient(to right, rgba(0, 0, 0, 0) 0%, "
            + "rgba(0, 0, 0, 0.6) 50%, rgba(0, 0, 0, 0) 100%);"
            + "-fx-effect: dropshadow(gaussian, rgba(0,0,0,0.7), 5, 0, 0, 2);"
            + "-fx-font-size: " + fontSettings.getFontSize() + ";"
            + "-fx-font-weight: " + fontSettings.getFontWeightString()
        );
      });

      // Set button style on mouse pressed
      button.setOnMousePressed(e -> {
        button.setStyle("-fx-background-color: linear-gradient(to left, "
            + "rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.6) 50%, rgba(0, 0, 0, 0) 100%), "
            + "linear-gradient(to right, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.6) 50%,"
            + " rgba(0, 0, 0, 0) 100%);"
            + "-fx-effect: dropshadow(gaussian, rgba(0,0,0,0.8), 6, 0, 0, 3);"
            + "-fx-font-size: " + fontSettings.getFontSize() + ";"
            + "-fx-font-weight: " + fontSettings.getFontWeightString()
        );
      });

      // Set button style on mouse released
      button.setOnMouseReleased(e -> {
        button.setStyle("-fx-background-color: linear-gradient(to left, "
            + "rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.6) 50%, rgba(0, 0, 0, 0) 100%), "
            + "linear-gradient(to right, rgba(0, 0, 0, 0) 0%, "
            + "rgba(0, 0, 0, 0.6) 50%, rgba(0, 0, 0, 0) 100%);"
            + "-fx-effect: dropshadow(gaussian, rgba(0,0,0,0.8), 6, 0, 0, 3);"
            + "-fx-font-size: " + fontSettings.getFontSize() + ";"
            + "-fx-font-weight: " + fontSettings.getFontWeightString()
        );
      });

      // Set button style on mouse exit
      button.setOnMouseExited(e -> {
        fadeTransition.playFromStart();
        button.setStyle("-fx-font-size: " + fontSettings.getFontSize() + ";"
            + "-fx-font-weight: " + fontSettings.getFontWeightString()
        );
      });
    }

    // Create parent nodes and scene
    // Create title and subtitle
    Label title = new Label("P A T H S");
    VBox titleAndSeparator = new VBox(title, new Separator());
    HBox titlesContainer = new HBox(titleAndSeparator);
    VBox menuButtons = new VBox(newGame, loadGame, options, quit);
    VBox menuButtonsContainer = new VBox(menuButtons);

    VBox layout = new VBox(titlesContainer, menuButtonsContainer);
    //Create MenuBar
    MenuBar menuBar = createMenuBar();


    // Set the alignment of the nodes and border pane layout
    StackPane.setAlignment(menuBar, Pos.TOP_LEFT);
    StackPane.setAlignment(layout, Pos.CENTER);
    AnchorPane.setTopAnchor(layout, 180.0);
    AnchorPane.setLeftAnchor(layout, 60.0);
    titlesContainer.setAlignment(Pos.CENTER_LEFT);

    //Set css-ids for the nodes and add the stylesheet to this scene
    menuBar.setId("menu-bar");
    title.setId("title");
    titleAndSeparator.setId("title-and-separator");
    menuButtons.setId("menu-buttons");
    layout.setId("layout");
    AnchorPane anchorPane = new AnchorPane(layout);
    anchorPane.setId("anchor-pane");
    StackPane root = new StackPane(anchorPane, menuBar);
    Scene scene = new Scene(root);
    scene.getStylesheets().add(Objects.requireNonNull(getClass()
        .getResource("/css/title-scene.css")).toExternalForm());

    // Set focus on the menu bar when the user has traversed through the menu buttons
    scene.focusOwnerProperty().addListener((obs, oldVal, newVal) -> {
      if (oldVal == quit) {
        menuBar.requestFocus();
      }
    });

    // Ensure that the current font settings are applied
    updateFont();

    // Set the scene and show the stage
    stage.setTitle("Paths - Main Menu");
    stage.setScene(scene);
    stage.show();
  }

  /**
   * Creates the menu bar for the view.
   *
   * @return the menu bar
   */
  private MenuBar createMenuBar() {

    //Create menubar and menu items
    MenuItem helpMenuItem = new MenuItem("Help");
    helpMenuItem.setOnAction(e -> controller.showHelp());

    Menu helpMenu = new Menu("Show help", null, helpMenuItem);

    return new MenuBar(helpMenu);
  }


  /**
   * Updates the view based on the font size setting.
   */
  @Override
  public void updateFont() {
    String fontStyle = "-fx-font-size: " + fontSettings.getFontSize()
        + "px; -fx-font-weight: " + fontSettings.getFontWeightString();

    newGame.setStyle(fontStyle);
    loadGame.setStyle(fontStyle);
    options.setStyle(fontStyle);
    quit.setStyle(fontStyle);
  }
}
