package edu.ntnu.idatt2001.mappevurdering.ej.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/** Represents a player in a story. */
public class Player implements Serializable {
  private final String name;
  private int health;
  private int score;
  private int gold;
  private final List<String> inventory;

  /**
   * Using a Builder pattern to create a new Player.
   */
  public static class Builder {
    //required parameters
    private final String name;

    //optional parameters
    private int health = 100;
    private int score = 0;
    private int gold = 0;
    private List<String> inventory = new ArrayList<>();

    /**
     * adds a name to the player using the builder pattern.
     *
     * @param name the name of the player
     */
    public Builder(String name) {
      this.name = name;
    }

    /**
     * adds health to the player using the builder pattern.
     *
     * @param value the amount of health to add
     *
     * @return the builder
     */
    public Builder health(int value) {
      this.health = value;
      return this;
    }

    /**
     * adds score to the player using the builder pattern.
     *
     * @param value the amount of score to add
     *
     * @return the builder
     */
    public Builder score(int value) {
      this.score = value;
      return this;
    }

    /**
     * adds gold to the player using the builder pattern.
     *
     * @param value the amount of gold to add
     *
     * @return the builder
     */
    public Builder gold(int value) {
      this.gold = value;
      return this;
    }

    /**
     * adds inventory to the player using the builder pattern.
     *
     * @param list the inventory to add
     *
     * @return the builder
     */
    public Builder inventory(ArrayList<String> list) {
      this.inventory = list;
      return this;
    }

    /**
     * builds the player.
     *
     * @return the player
     */
    public Player build() {
      return new Player(this);
    }

  }

  /**
   * Creates a new Player from the builder.
   *
   * @param builder the builder to use
   */
  private Player(Builder builder) {
    name = builder.name;
    health = builder.health;
    score = builder.score;
    gold = builder.gold;
    inventory = builder.inventory;
  }

  /**
   * gets the name of the player.
   *
   * @return  the name of the player
   */
  public String getName() {
    return name;
  }

  /**
   * gets the health of the player.
   *
   * @return the health of the player
   */
  public int getHealth() {
    return health;
  }

  /**
   * gets the score of the player.
   *
   * @return the score of the player
   */
  public int getScore() {
    return score;
  }

  /**
   * gets the gold of the player.
   *
   * @return the gold of the player
   */
  public int getGold() {
    return gold;
  }

  /**
   * gets the inventory of the player.
   *
   * @return the inventory of the player
   */
  public List<String> getInventory() {
    return inventory;
  }

  /**
   * sets the health of the player.
   *
   * @param health the health to set
   */
  public void addHealth(int health) {
    this.health += health;
    if (this.health < 0) {
      this.health = 0;
    }
  }

  /**
   * sets the score of the player.
   *
   * @param score the score to set
   */
  public void addScore(int score) {
    this.score += score;
    if (this.score < 0) {
      this.gold = 0;
    }
  }

  /**
   * sets the gold of the player.
   *
   * @param gold the gold to set
   */
  public void addGold(int gold) {
    this.gold += gold;
    if (this.gold < 0) {
      this.gold = 0;
    }
  }

  /**
   * adds an item to the inventory of the player.
   *
   * @param item the item to add
   */
  public void addToInventory(String item) {
    this.inventory.add(item);
  }
}
