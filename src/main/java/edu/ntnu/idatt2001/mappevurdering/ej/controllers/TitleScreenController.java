package edu.ntnu.idatt2001.mappevurdering.ej.controllers;

import edu.ntnu.idatt2001.mappevurdering.ej.utils.GraphicalUtils;
import edu.ntnu.idatt2001.mappevurdering.ej.view.TitleScreenView;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

/**
 * Controller class for the title screen.
 *
 * @version 2021-05-17
 *
 * @author joachimgw
 */
public class TitleScreenController {

  private final Stage stage;


  /**
   * Constructor for TitleScreenController.
   *
   * @param stage the stage to display the view on.
   */
  public TitleScreenController(Stage stage) {
    this.stage = stage;
  }


  /**
   * Initializes the view.
   */
  public void initialize() {
    TitleScreenView view = new TitleScreenView(stage, this);
    view.setup();
  }


  /**
   * Initializes the new game view.
   */
  public void newGame() {
    NewGameController controller = new NewGameController(stage, this);
    controller.initialize();
  }


  /**
   * Initializes the load game view.
   */
  public void loadGame() {
    LoadGameController controller = new LoadGameController(stage, this);
    controller.initialize();
  }


  /**
   * Initializes the options view.
   */
  public void openOptions() {
    OptionsController controller = new OptionsController(stage, this);
    controller.initialize();
  }


  /**
   * Exits the application.
   */
  public void exit() {
    GraphicalUtils.getInstance().closeProgram(stage);
  }



  /**
   * Shows the help popup.
   */
  public void showHelp() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle("Title Screen Help");
    alert.setHeaderText(null);
    alert.setContentText("""
        Welcome to the Title Screen!

        This screen allows you to access various options:
        - New Game: Start a new game
        - Load Game: Load a previously saved game
        - Options: Open the options menu
        - Quit: Exit the application
        """);

    alert.showAndWait();
  }
}
