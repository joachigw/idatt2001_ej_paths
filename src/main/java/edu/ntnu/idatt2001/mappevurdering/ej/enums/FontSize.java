package edu.ntnu.idatt2001.mappevurdering.ej.enums;

/**
 * An enum for font sizes.
 */
public enum FontSize {
  SMALL(14),
  MEDIUM(20),
  LARGE(24);

  private final int size;


  /**
   * Creates a new FontSize.
   *
   * @param size the size of the font.
   */
  FontSize(int size) {
    this.size = size;
  }


  /**
   * Returns the size of the font.
   *
   * @return The size of the font.
   */
  public int getSize() {
    return size;
  }
}
