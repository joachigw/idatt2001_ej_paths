package edu.ntnu.idatt2001.mappevurdering.ej;

import edu.ntnu.idatt2001.mappevurdering.ej.controllers.TitleScreenController;
import javafx.application.Application;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * The main class of the application.
 * Used to start the application.
 */
public class MainApp extends Application {


  /**
   * The start method of the application.
   *
   * @param stage the stage to start
   */
  @Override
  public void start(Stage stage) {

    // Set the stage to initially be 80% of the screen size
    stage.setHeight(Screen.getPrimary().getBounds().getHeight() * 0.8);
    stage.setWidth(Screen.getPrimary().getBounds().getWidth() * 0.8);

    // Create a new TitleScreenController and initialize it
    TitleScreenController controller = new TitleScreenController(stage);
    controller.initialize();
  }


  /**
   * The main method of the application.
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    launch(args);
  }
}
