package edu.ntnu.idatt2001.mappevurdering.ej.models;

import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.Action;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Represents a link in a passage.
 */
public class Link implements Serializable {
  private final String text;
  private final String reference;
  private final List<Action> actions;

  /**
   * Creates a new Link.
   *
   * @param text the text to display.
   *
   * @param reference the reference to the next room.
   *
   * @param actions the actions to execute.
   */
  public Link(String text, String reference, List<Action> actions) {
    this.text = text;
    this.reference = reference;
    this.actions = actions;
  }

  /**
   * gets the link text.
   *
   * @return the link text.
   */
  public String getText() {
    return text;
  }

  /**
   * gets the link reference.
   *
   * @return the link reference.
   *
   */
  public String getReference() {
    return reference;
  }

  /**
   * gets the link actions.
   *
   * @return the link actions
   */
  public List<Action> getActions() {
    return actions;
  }

  /**
   * adds an action to the link.
   *
   * @param action the action to add.
   *
   * @return true if the action was added, false if not
   */
  public boolean addAction(Action action) {
    return actions.add(action);
  }


  /**
   * gets only the link text to string.
   *
   * @return the link text
   */
  @Override
  public String toString() {
    return text;
  }

  /**
   * gets the link text and reference to string.
   *
   * @return the link text and reference
   */
  public String toSmallString() {
    return  "[" + text + "] (" + reference + ")";
  }


  /**
   * asserts if an object is equal to this class and if it's an equal link.
   *
   * @param o the object to compare.
   *
   * @return boolean
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Link link = (Link) o;
    return text.equals(link.text) && reference.equals(link.reference)
            && (actions == null || actions.equals(link.actions));
  }

  /**
   * gets the hashcode of the link.
   *
   * @return the hashcode of the link
   *
   */
  @Override
  public int hashCode() {
    return Objects.hash(text, reference, actions);
  }

  /**
   * gets the link to string.
   *
   * @return the link to string.
   */
  public String toFileString() {
    StringBuilder brokenLinks = new StringBuilder();
    for (Action action : actions) {
      brokenLinks.append(action.toFileString());
    }
    return  "[" + text + "] (" + reference + ") {" + brokenLinks + "}";
  }
}
