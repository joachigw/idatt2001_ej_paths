package edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal;

import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import java.io.Serializable;


/**
 * Represents a goal to reach a certain amount of gold.
 */
public class GoldGoal implements Goal, Serializable {

  private final int goldGoal;


  /**
   * Creates a new GoldGoal.
   *
   * @param goldGoal the amount of gold to reach.
   */
  public GoldGoal(int goldGoal) {
    this.goldGoal = goldGoal;
  }


  /**
   * Checks if the goal is fulfilled.
   *
   * @param player the player to check.
   * @return true if the goal is fulfilled, false otherwise
   */
  @Override
  public boolean isFulfilled(Player player) {
    return player.getGold() >= goldGoal;
  }


  /**
   * Returns a string representation of the goal.
   *
   * @return a string representation of the goal.
   */
  @Override
  public String toString() {
    return "Gold Goal: reach '" + goldGoal + "' gold.";
  }
}
