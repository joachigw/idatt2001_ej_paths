package edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action;

import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import java.io.Serializable;


/**
 * Represents an action that adds or removes the player's score.
 */
public class ScoreAction implements Action, Serializable {

  private final int score;


  /**
   * Creates a new ScoreAction.
   *
   * @param score the amount of score to add.
   */
  public ScoreAction(int score) {
    this.score = score;
  }


  /**
   * Executes the action.
   *
   * @param player the player to execute the action on.
   */
  @Override
  public void execute(Player player) {
    player.addScore(this.score);
  }


  /**
   * Returns a string representation of the action.
   *
   * @return a string representation of the action.
   */
  @Override
  public String toString() {
    if (this.score < 0) {
      String negative = String.valueOf(this.score);
      return "You lost: " + negative.replace("-", "") + " score...";
    } else {
      return "You gained: " + this.score + " score!";
    }
  }


  /**
   * Returns a string representation of the action.
   *
   * @return a string representation of the action.
   */
  public String toFileString() {
    return "ScoreAction(" + this.score + ")";
  }
}
