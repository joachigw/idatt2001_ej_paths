package edu.ntnu.idatt2001.mappevurdering.ej.utils;

import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.FontObserver;
import java.util.ArrayList;
import java.util.List;

/**
 * Subject class for the Observer pattern.
 * This class is responsible for notifying all FontObservers when a change has been made.
 * This class is also responsible for attaching and detaching FontObservers.
 * This class is also responsible for keeping track of all FontObservers.
 */
public class Subject {

  List<FontObserver> fontObservers;


  /**
   * Constructor for Subject.
   */
  public Subject() {
    this.fontObservers = new ArrayList<>();
  }


  /**
   * Attach a FontObserver to the Subject.
   *
   * @param fontObserver FontObserver to attach.
   */
  public void attach(FontObserver fontObserver) {
    fontObservers.add(fontObserver);
  }


  /**
   * Detach a FontObserver from the Subject.
   *
   * @param fontObserver FontObserver to detach.
   */
  public void detach(FontObserver fontObserver) {
    fontObservers.remove(fontObserver);
  }


  /**
   * Notify all FontObservers attached to the Subject.
   */
  public void notifyObservers() {
    fontObservers.forEach(FontObserver::updateFont);
  }
}
