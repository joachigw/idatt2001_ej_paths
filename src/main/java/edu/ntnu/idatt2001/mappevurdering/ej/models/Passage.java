package edu.ntnu.idatt2001.mappevurdering.ej.models;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/** Represents a passage in a story. */
public class Passage implements Serializable {
  String title;
  String content;
  List<Link> links;

  /**
   * Creates a new Passage.
   *
   * @param title the title of the passage
   *
   * @param content the content of the passage
   *
   * @param links the links of the passage
   */
  public Passage(String title, String content, List<Link> links) {
    this.title = title;
    this.content = content;
    this.links = links;
  }

  /**
   * gets the passage title.
   *
   * @return the passage title
   */
  public String getTitle() {
    return title;
  }

  /**
   * sets the passage title.
   *
   * @param newTitle the new passage title.
   */
  public void setTitle(String newTitle) {
    this.title = newTitle;
  }

  /**
   * gets the passage content.
   *
   * @return the passage content
   */
  public String getContent() {
    return content;
  }

  /**
   * sets the passage content.
   *
   * @param newContent the new passage content
   */
  public void setContent(String newContent) {
    this.content = newContent;
  }

  /**
   * gets the passage links.
   *
   * @return the passage links
   */
  public List<Link> getLinks() {
    return links;
  }

  /**
   * adds a link to the passage.
   *
   * @param link the link to add
   */
  public void addLink(Link link) {
    this.links.add(link);
  }

  /**
   * checks if the passage has links.
   *
   * @return true if the passage has links, false if not
   */
  public boolean hasLinks() {
    return links.size() > 0;
  }

  /**
   * checks if the object is equal to this passage object.
   *
   * @param o the object to check
   *
   * @return true if the object is equal to this passage object, false if not
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Passage passage = (Passage) o;
    return title.equals(passage.title) && content.equals(passage.content)
        && links.equals(passage.links);
  }


  /**
   * gets the hashcode of the passage.
   *
   * @return the hashcode of the passage
   */
  @Override
  public int hashCode() {
    return Objects.hash(title, content, links);
  }

  /**
   * gets the passage to string.
   *
   * @return the passage to string
   */
  @Override
  public String toString() {
    return "Passage{"
        + "title='" + title + '\''
        + ", content='" + content + '\''
        + ", links=" + links + '}' + "\n";
  }
}
