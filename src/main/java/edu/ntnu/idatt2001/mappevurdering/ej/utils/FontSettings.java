package edu.ntnu.idatt2001.mappevurdering.ej.utils;

import edu.ntnu.idatt2001.mappevurdering.ej.enums.FontSize;

/**
 * A singleton class used to store the font settings.
 * The font settings are used to change the font size and style of the text in the application.
 * The class is observable, and notifies its fontObservers when the font settings are changed.
 *
 * @version 2023-05-21
 * @author joachimgw
 */
public class FontSettings extends Subject {

  private int size;
  private FontSize fontSizeEnum;
  private boolean isBold;
  private static FontSettings instance;


  /**
   * Private constructor to prevent instantiation outside of this class.
   */
  private FontSettings() {
    this.size = FontSize.MEDIUM.getSize();
    this.fontSizeEnum = FontSize.MEDIUM;
    this.isBold = false;
  }


  /**
   * Returns the instance of the FontSettings, or creates a new one if it doesn't exist.
   *
   * @return the instance of the FontSettings.
   */
  public static FontSettings getInstance() {
    if (instance == null) {
      instance = new FontSettings();
    }
    return instance;
  }


  /**
   * Returns the font size.
   *
   * @return the font size.
   */
  public int getFontSize() {
    return size;
  }


  /**
   * Returns the font size as an enum.
   *
   * @return the font size as an enum.
   */
  public FontSize getFontSizeEnum() {
    return fontSizeEnum;
  }


  /**
   * Returns the font weight as a string.
   *
   * @return the font weight as a string.
   */
  public String getFontWeightString() {
    if (isBold) {
      return "bold";
    } else {
      return "normal";
    }
  }


  /**
   * Sets the font size (and enum value) and notifies fontObservers.
   *
   * @param fontSize the new font size.
   */
  public void setFontSize(FontSize fontSize) {
    this.size = fontSize.getSize();
    this.fontSizeEnum = fontSize;
    notifyObservers();
  }


  /**
   * Sets the font weight and notifies fontObservers.
   *
   * @param isBold the new font weight.
   */
  public void setIsBold(boolean isBold) {
    this.isBold = isBold;
    notifyObservers();
  }


  /**
   * Returns the boolean value of the checkbox.
   *
   * @return the boolean value of the checkbox.
   */
  public boolean getIsBold() {
    return isBold;
  }
}
