package edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action;

import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import java.io.Serializable;


/**
 * Represents an action that adds or removes the player's health.
 */
public class HealthAction implements Action, Serializable {

  private final int health;

  /**
   * Creates a new HealthAction.
   *
   * @param health the amount of health to add.
   */
  public HealthAction(int health) {
    this.health = health;
  }


  /**
   * Executes the action.
   *
   * @param player the player to execute the action on.
   */
  @Override
  public void execute(Player player) {
    player.addHealth(this.health);
  }


  /**
   * Returns a string representation of the action.
   *
   * @return a string representation of the action.
   */
  @Override
  public String toString() {
    if (this.health < 0) {
      String negative = String.valueOf(this.health);
      return "You lost: " + negative.replace("-", "") + " health...";
    } else {
      return "You gained: " + this.health + " health!";
    }
  }


  /**
   * Returns a string representation of the action.
   *
   * @return a string representation of the action.
   */
  public String toFileString() {
    return "HealthAction(" + this.health + ")";
  }
}
