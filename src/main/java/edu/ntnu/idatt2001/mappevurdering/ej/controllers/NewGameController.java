package edu.ntnu.idatt2001.mappevurdering.ej.controllers;

import edu.ntnu.idatt2001.mappevurdering.ej.filehandling.ReadFile;
import edu.ntnu.idatt2001.mappevurdering.ej.filehandling.WriteFile;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.Goal;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.GoldGoal;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.HealthGoal;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.InventoryGoal;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.ScoreGoal;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Game;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Link;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Passage;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Story;
import edu.ntnu.idatt2001.mappevurdering.ej.view.NewGameView;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Popup;
import javafx.stage.Stage;


/**
 * Controller class for the NewGameView.
 */
public class NewGameController {

  private final Stage stage;
  private final TitleScreenController titleScreenController;

  private final ReadFile reader = new ReadFile();
  private final WriteFile writer = new WriteFile();
  private NewGameView view;
  private File selectedFile;

  /**
   * Constructor for LoadGameController.
   *
   * @param stage the stage to display the view on
   */
  public NewGameController(Stage stage, TitleScreenController titleScreenController) {

    this.stage = stage;
    this.titleScreenController = titleScreenController;
  }

  public void initialize() {
    view = new NewGameView(stage, this);
    view.setup();
  }


  private Boolean validateInput() {
    boolean checker = true;

    TextField[] integerInputs = {view.getPlayerHealthField(),
        view.getPlayerGoldField(), view.getPlayerScoreField()};

    //checks the game name input
    TextField gameName = view.getGameNameField();
    String alertPromptColor = "-fx-prompt-text-fill: red;";
    if (gameName.getText().isEmpty()) {
      gameName.setStyle(alertPromptColor + "-fx-alignment: center;");
      checker = false;
    } else if (gameName.getText().matches(".*[/\\\\:*?\"<>|\\x00].*")) {
      gameName.clear();
      gameName.setPromptText("Name contains restricted characters.");
      gameName.setStyle(alertPromptColor + "-fx-alignment: center");
    }


    //checks player name input
    TextField playerName = view.getPlayerNameField();
    if (playerName.getText().isEmpty()) {
      playerName.setPromptText("Please Fill in a Name");
      playerName.setStyle(alertPromptColor);
      checker = false;
    } else if (playerName.getText().matches(".*\\d+.*")) {
      playerName.clear();
      playerName.setPromptText("Please dont use numbers");
      playerName.setStyle("-fx-prompt-text-fill: red");
      checker = false;
    }


    //Check Player Integer Inputs
    for (TextField field : integerInputs) {
      if (!field.getText().isEmpty() && !field.getText().matches("\\d+")) {
        field.clear();
        //Non integer found
        field.setPromptText("Invalid Input. Please input a whole number without Letters.");
        field.setStyle(alertPromptColor);
        checker = false;
      } else if (!field.getText().isEmpty()) {
        int value = Integer.parseInt(field.getText().replace(" ", ""));
        if (value < 1) {
          //Value is not greater than 0
          if (field.equals(view.getPlayerScoreField()) && value < 0) {
            field.clear();
            field.setPromptText("Invalid Input. The Number must be above or equall to 0");
            field.setStyle(alertPromptColor);
            checker = false;
          } else if (field.equals(view.getPlayerGoldField()) && value < 0) {
            field.clear();
            field.setPromptText("Invalid Input. The Number must be above or equall to 0");
            field.setStyle(alertPromptColor);
            checker = false;
          } else if (field.equals(view.getPlayerHealthField())) {
            field.clear();
            field.setPromptText("Invalid Input. The Number must be above 0");
            field.setStyle(alertPromptColor);
            checker = false;
          }
        }
      }
    }

    //Checks Paths Choice
    if (selectedFile == null) {
      view.getStoryChoice().setStyle("-fx-text-fill: red;");
      view.getStoryContent().setStyle("-fx-text-fill: red;");
      view.getStoryContent().setContent(new TextFlow(
              new Text("Error: No file chosen. Please choose a .paths file")));

      view.getLinkAlert().setText("");
      view.getLinkBtn().setVisible(false);
      checker = false;
    }

    return checker;
  }


  /**
   * Method for assigning the action to the create game button "NEXT".
   */
  public void assign_NextB_Action(Button nextB) throws IOException {
    if (validateInput()) {
      Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
      File file = new File("src/main/resources/1_Games/"
          + view.getGameNameField().getText() + ".game");
      if (file.exists()) {
        confirmation.setHeaderText("Game already exists");
        confirmation.setContentText("A game with the name "
            + view.getGameNameField().getText() + " already exists. Do you want to overwrite it?");
      } else {
        confirmation.setHeaderText("Confirm Game");
        confirmation.setContentText("Are you all set to create a game?");
      }

      Optional<ButtonType> result = confirmation.showAndWait();
      if (result.isPresent() && result.get() == ButtonType.OK) {

        int playerHealth;
        if (view.getPlayerHealthField().getText().isEmpty()) {
          playerHealth = 100;
        } else {
          playerHealth = Integer.parseInt(view.getPlayerHealthField().getText());
        }
        int playerGold;
        if (view.getPlayerGoldField().getText().isEmpty()) {
          playerGold = 0;
        } else {
          playerGold = Integer.parseInt(view.getPlayerGoldField().getText());
        }
        int playerScore;
        if (view.getPlayerScoreField().getText().isEmpty()) {
          playerScore = 0;
        } else {
          playerScore = Integer.parseInt(view.getPlayerScoreField().getText());
        }

        Player player = new Player.Builder(view.getPlayerNameField().getText())
                  .gold(playerGold)
                  .health(playerHealth)
                  .score(playerScore)
                  .inventory(new ArrayList<>(view.getPlayerInventoryList().getItems()))
                  .build();

        Map<Link, Passage> passages = new HashMap<>();
        Passage opPassage = reader.readPassage("First Passage", selectedFile);
        Link opLink = new Link("Opening Link", opPassage.getTitle(), null);
        passages.put(opLink, opPassage);
        Story story = new Story(selectedFile.getName(), passages, selectedFile);
        List<Goal> goals = new ArrayList<>(view.getGoalList().getItems());

        Game newGame = new Game(view.getGameNameField().getText(), player,
            story, goals, opPassage, opLink);
        writer.saveGame(newGame);
        nextB.setVisible(false);
        view.getUserFeedbackLabel().setText("Game was created! Go to Load Game and look for '"
                          + newGame.getTitle() + "' to play!");
      }
    }
  }


  public void returnButtonClicked() {
    titleScreenController.initialize();
  }


  /**
   * Method for assigning the action to the show help button.
   */
  public void showHelp() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle("New Game Help");
    alert.setHeaderText(null);
    alert.setContentText("""
        New Game View Help
                
        Welcome to the New Game View! This is where you can create a\s
         new game and set up its details.\s
         Here's a guide to help you understand the features and how to use them:
             
                
        Game Name:
                
        In the text field labeled "Game Name," enter the name for your game.
        
        
        Player Information:
                
        In the "PLAYER" section, you can define the details of the main player character.
        Enter the character's name in the "Name" field.
        Optionally, you can specify the character's health, gold,\s
         and score in their respective fields.
        To manage the character's inventory, click the "Add item" \s
        button and select items from the list.
        To remove an item from the inventory, right-click on the \s
        item and choose "Remove."
        
        
        Story Selection:
                
        In the "STORY" section, you can choose a story for your game.
        Click the "Choose a Story from File" button to select a story file.
        Once you've selected a story, the description of the story's first\s
         passage will be displayed in the content window.
        If there are any broken links in the story, an alert will be shown. \s
        You can click the "SHOW" button to view the broken links.
        
        
        Goals:
                
        In the "GOALS" section, you can define goals for your game.
        Click the "Add goal" button to add a new goal.
        The list of goals will be displayed, and you can scroll through them.
        To remove a goal, right-click on it and choose "Remove."
        
        
        Navigation:
                
        To navigate back to the main menu, click the "<- Return to main menu" button.
        When you're done setting up the game, click the "NEXT" button to proceed.
        
        
        That's it! Now you know how to use the New Game View\s
         to create a new game and customize its settings.\s
         Enjoy creating your game and have fun playing!""");

    alert.getDialogPane().setPrefWidth(900);
    alert.showAndWait();
  }


  /**
   * Method for assigning the action to the add to inventory button.
   */
  public void assignPlayerInventoryAdd_Action() {
    Dialog<ButtonType> dialog = new Dialog<>();
    dialog.setTitle("Add Inventory Item");
    dialog.setHeaderText("Please input the name of the item:");
    dialog.setContentText("Name:");

    TextField input = new TextField();
    input.setPromptText("Inventory Item Name");
    GridPane.setConstraints(input, 0, 0, 2, 1);

    Button addBtn = new Button("Add");
    addBtn.setDefaultButton(true);

    Label errorMsg = new Label("");
    GridPane.setConstraints(errorMsg, 0, 1, 2, 1);
    errorMsg.setStyle("-fx-text-fill: red;");

    // Create the layout for the dialog content
    GridPane dialogContent = new GridPane();
    dialogContent.setHgap(10);
    dialogContent.setVgap(10);
    dialogContent.setPadding(new Insets(10));
    dialogContent.addColumn(0, input);
    dialogContent.addColumn(2, addBtn);
    dialogContent.addRow(1, errorMsg);


    dialog.getDialogPane().setContent(dialogContent);


    dialog.getDialogPane().getScene().getWindow()
      .setOnCloseRequest(e -> dialog.setResult(ButtonType.CANCEL));


    ListView<String> inventoryList = view.getPlayerInventoryList();
    addBtn.setOnAction(e -> {
      if (input.getText().isEmpty()) {
        errorMsg.setText("Please input a item name");
        return;

      }  else if (input.getText().matches(".*\\d+.*")) {
        input.clear();
        errorMsg.setText("Please enter a name without any numbers");
        return;
      }
      inventoryList.getItems().add(input.getText());
      inventoryList.refresh();

      dialog.setResult(ButtonType.OK);
      dialog.close();
    });

    dialog.showAndWait();
  }

  /**
   * Method for assigning the action to the add story button.
   */
  public void assignStoryAdd_Action() throws IOException {
    Button storyButton = view.getStoryChoice();
    ScrollPane storyContent = view.getStoryContent();
    //File chooser to choose story/script
    storyButton.setStyle("");
    storyContent.setStyle("");
    FileChooser fileChooser = new FileChooser();
    fileChooser.setInitialDirectory(new File("src/main/resources/0_Scripts"));
    selectedFile = fileChooser.showOpenDialog(stage);

    if (selectedFile != null && !selectedFile.getName().contains(".paths")) {
      storyButton.setText("Choose a Story from File");
      storyContent.setContent(new TextFlow(
          new Text("Error: Please choose a file that ends in '.paths'")));
      storyContent.setStyle("-fx-text-fill: red");
    } else if (selectedFile == null) {
      storyButton.setText("Choose a Story from File");
      storyContent.setContent(new TextFlow(
          new Text("Error: No file was chosen")));
      storyContent.setStyle("-fx-text-fill: red");
    } else {
      storyButton.setText(selectedFile.getName());
      //Clears style in case of error
      storyButton.setStyle("");

      storyContent.setContent(new TextFlow(
          new Text(reader.readPassage("First Passage", selectedFile).getContent())));

      if (reader.getBrokenLinks(selectedFile).contains("There are broken links")) {
        view.getLinkBtn().setVisible(true);
        view.getLinkBtn().setOnAction(e -> showBrokenLinks());
        view.getLinkAlert().setText("Script contains broken link: ");
      }

    }

  }

  private void showBrokenLinks() {
    Popup popup = new Popup();

    Label brokenLinksTitle = new Label("Broken Links");
    brokenLinksTitle.setId("broken-links-title");
    HBox centeredTitle = new HBox(brokenLinksTitle);
    centeredTitle.setAlignment(Pos.CENTER);

    Button closeBtn = new Button("Close");
    closeBtn.setOnAction(e -> popup.hide());

    HBox centeredBtn = new HBox(closeBtn);
    centeredBtn.setAlignment(Pos.CENTER);

    TextArea goalReached = new TextArea(reader.getBrokenLinks(selectedFile));
    goalReached.setEditable(false);
    ScrollPane scroll = new ScrollPane(goalReached);
    scroll.setFitToWidth(true);


    VBox box = new VBox(centeredTitle, scroll, centeredBtn);
    box.setStyle("-fx-background-color: white; -fx-border-color: black; -fx-border-width: 1px;");

    popup.getContent().addAll(box);
    popup.show(stage);
  }

  /**
   * Method for assigning the action to the add goal button.
   */
  public void assignGoalsAdd_Action() {
    Dialog<ButtonType> dialog = new Dialog<>();
    dialog.setTitle("Add a Goal to Story");
    dialog.setHeaderText("Insert the goal specifications:");

    //Goal version choice
    ComboBox<String> goalVersion = new ComboBox<>();
    goalVersion.setPromptText("Goal Type");
    goalVersion.getItems().addAll("Gold Goal",
        "Health Goal",
        "Inventory Goal Item",
        "Inventory Goal Size",
        "Score Goal");

    TextField input = new TextField();


    goalVersion.getSelectionModel().selectedItemProperty()
        .addListener((ObservableValue<? extends String> observable,
            String oldValue, String newValue) -> {
          // Update the prompt text based on the selected choice
          if (newValue.equals("Inventory Goal Item")) {
            input.setPromptText("Enter Item Name");
          } else {
            input.setPromptText("Enter Goal Value");
          }
        });

    Button addBtn = new Button("ADD");
    addBtn.setDefaultButton(true);

    Label errorMsg = new Label("Remember: Dont make a goal that is completed on game start!");
    GridPane.setConstraints(errorMsg, 0, 1, 2, 1);


    // Create the layout for the dialog content
    GridPane dialogContent = new GridPane();
    dialogContent.setHgap(10);
    dialogContent.setVgap(10);
    dialogContent.setPadding(new Insets(10));

    dialogContent.addColumn(0, goalVersion);
    dialogContent.addColumn(1, input);
    dialogContent.addColumn(2, addBtn);
    dialogContent.addRow(1, errorMsg);

    dialog.getDialogPane().setContent(dialogContent);



    dialog.getDialogPane().getScene().getWindow().setOnCloseRequest(e ->
        dialog.setResult(ButtonType.CANCEL));

    //Handle Button Clicked
    addBtn.setOnAction(e -> {
      if (goalVersion.getValue() == null) {
        errorMsg.setText("Please select a goal version.");
        errorMsg.setStyle("-fx-text-fill: red;");
        return;
      }

      String version = goalVersion.getValue().replace(" ", "");
      String inputText = input.getText();
      ListView<Goal> goalList = view.getGoalList();
      try {
        if (version.equals("GoldGoal")) {
          int value = Integer.parseInt(inputText);
          if (!(value > 0)) {
            errorMsg.setText("Please input a number bigger than 0");
            errorMsg.setStyle("-fx-text-fill: red;");
            return;
          }
          GoldGoal goal = new GoldGoal(value);
          goalList.getItems().add(goal);
        }
        if (version.equals("HealthGoal")) {
          int value = Integer.parseInt(inputText);
          if (!(value > 0)) {
            errorMsg.setText("Please input a number bigger than 0");
            errorMsg.setStyle("-fx-text-fill: red;");
            input.clear();
            return;
          }
          HealthGoal goal = new HealthGoal(value);
          goalList.getItems().add(goal);
        }
        if (version.equals("ScoreGoal")) {
          int value = Integer.parseInt(inputText);
          if (!(value > 0)) {
            errorMsg.setText("Please input a number bigger than 0");
            errorMsg.setStyle("-fx-text-fill: red;");
            input.clear();
            return;
          }
          ScoreGoal goal = new ScoreGoal(value);
          goalList.getItems().add(goal);
        }
        if (version.equals("InventoryGoalItem")) {
          if (input.getText().isEmpty()) {
            errorMsg.setText("Please enter a name for the item");
            errorMsg.setStyle("-fx-text-fill: red;");
            input.clear();
            return;
          } else if (inputText.matches(".*\\d+.*")) {
            errorMsg.setText("Please enter a name without any numbers");
            errorMsg.setStyle("-fx-text-fill: red;");
            input.clear();
            return;
          }
          InventoryGoal goal = new InventoryGoal(input.getText());
          goalList.getItems().add(goal);
        }

        if (version.equals("InventoryGoalSize")) {

          int value = Integer.parseInt(input.getText());
          if (!(value > 0)) {
            errorMsg.setText("Please input a number bigger than 0");
            errorMsg.setStyle("-fx-text-fill: red;");
            input.clear();
            return;
          }
          InventoryGoal goal = new InventoryGoal(value);
          goalList.getItems().add(goal);
          input.clear();

        }
      } catch (NumberFormatException ex) {
        input.clear();
        errorMsg.setText("Please input a valid number");
        errorMsg.setStyle("-fx-text-fill: red;");
        return;
      }


      goalList.refresh();
      dialog.setResult(ButtonType.OK);
      dialog.close();

    });

    dialog.showAndWait();
  }

  /**
   * Method for assigning the action to delete goal button.
   */
  public void deleteGoalClick() {
    if (view.getGoalList().getSelectionModel().getSelectedItem() != null) {
      view.getGoalList().getItems().remove(view.getGoalList()
          .getSelectionModel().getSelectedItem());
    }
  }

  /**
   * Method for assigning the action to delete inventory item button.
   */
  public void deleteInventoryItemClick() {
    if (view.getPlayerInventoryList().getSelectionModel().getSelectedItem() != null) {
      view.getPlayerInventoryList().getItems().remove(view.getPlayerInventoryList()
          .getSelectionModel().getSelectedItem());
    }
  }
}

