package edu.ntnu.idatt2001.mappevurdering.ej.controllers;

import edu.ntnu.idatt2001.mappevurdering.ej.filehandling.ReadFile;
import edu.ntnu.idatt2001.mappevurdering.ej.filehandling.WriteFile;
import edu.ntnu.idatt2001.mappevurdering.ej.utils.GraphicalUtils;
import edu.ntnu.idatt2001.mappevurdering.ej.view.LoadGameView;
import java.io.File;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


/**
 * Controller class for the load game view.
 */
public class LoadGameController {

  private final Stage stage;
  private final TitleScreenController titleScreenController;
  private final GameController gameController;

  private final ReadFile reader = new ReadFile();


  /**
   * Constructor for LoadGameController.
   *
   * @param stage the stage to display the view on
   */
  public LoadGameController(Stage stage, TitleScreenController titleScreenController) {
    this.stage = stage;
    this.titleScreenController = titleScreenController;
    this.gameController = new GameController(stage, titleScreenController);
  }


  /**
   * Initializes the view.
   *
   * @see LoadGameView for more information.
   */
  public void initialize() {
    LoadGameView view = new LoadGameView(stage, this);
    view.setup();
  }


  /**
   * Returns to the title screen.
   */
  public void returnButtonClicked() {
    titleScreenController.initialize();
  }


  /**
   * Opens a file explorer window to browse local files.
   * Verifies that the chosen file is a .paths file.
   */
  public void browseButtonClicked() {

    // Opens a file explorer window
    File selectedFile = new FileChooser().showOpenDialog(stage);

    // Validates the chosen file
    if (selectedFile == null) {
      return;
    } else if (!selectedFile.getName().endsWith(".game")) {
      GraphicalUtils.getInstance().showAlert("Invalid file type. Please select a .game file.");
      return;
    }

    loadGame(selectedFile);
  }


  /**
   * Shows the help dialog for the load game screen.
   */
  public void showHelp() {
    Alert helpPopup = new Alert(Alert.AlertType.INFORMATION);
    helpPopup.setTitle("Load Game Help");
    helpPopup.setHeaderText("Instructions:");
    helpPopup.setContentText("""
            You can load a previously saved game or open a new one.


            Browse local files:
            Click on the 'Browse local files' button to view the locally saved game files.
            
            
            Select a game file:
            In the table, locate the game you want to load and click the 'Open' button.
            
            
            Delete a game file (optional):
            To delete a game file, click the 'Delete' button next to the game file.
            You will be prompted to confirm the deletion.
            Please note that this action is irreversible.


            Note: Only game files saved within the project's resources folder '1_Games'\s
             with the '.game' extension will be displayed in the table.
            """);

    helpPopup.showAndWait();
  }


  /**
   * Loads a game from a file.
   *
   * @param game the game file to load
   */
  public void loadGame(File game) {
    gameController.initialize(reader.readGame(game));
  }


  /**
   * Deletes a game file.
   *
   * @param game the game file to delete
   */
  public void deleteGame(File game) {
    WriteFile writer = new WriteFile();
    writer.removeGame(reader.readGame(game));
  }
}
