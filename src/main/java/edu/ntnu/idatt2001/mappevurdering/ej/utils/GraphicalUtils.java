package edu.ntnu.idatt2001.mappevurdering.ej.utils;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;


/**
 * Class for handling UI-related tasks.
 * Displays alerts and closes the program (with confirmation alert).
 *
 * @version 2021-05-18
 */
public class GraphicalUtils {

  private static GraphicalUtils instance;

  /**
   * Private constructor to ensure that the GraphicalUtils is a singleton.
   * Cannot be instantiated outside of this class.
   */
  private GraphicalUtils() {
  }


  /**
   * Returns the instance of the GraphicalUtils.
   *
   * @return the instance of the GraphicalUtils.
   */
  public static GraphicalUtils getInstance() {
    // Ensures that only one instance of the GraphicalUtils has been
    if (instance == null) {
      instance = new GraphicalUtils();
    }
    return instance;
  }


  /**
   * Displays an alert with the given title and message.
   *
   * @param title the title of the alert.
   * @param message the message of the alert.
   * @return true if the user confirms the action, false otherwise.
   */
  public boolean displayAlert(String title, String message) {
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    alert.setTitle(title);
    alert.setContentText(message);
    ButtonType okButton = new ButtonType("Yes, please exit", ButtonBar.ButtonData.OK_DONE);
    ButtonType cancelButton = new ButtonType("No, stay", ButtonBar.ButtonData.CANCEL_CLOSE);
    alert.getButtonTypes().setAll(okButton, cancelButton);
    Optional<ButtonType> result = alert.showAndWait();
    return result.isPresent() && result.get() == okButton;
  }


  /**
   * Shows an alert with the given message.
   *
   * @param message the message to show.
   */
  public void showAlert(String message) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText(null);
    alert.setContentText(message);
    alert.showAndWait();
  }


  /**
   * Closes the current stage if the user confirms the action.
   */
  public void closeProgram(Stage stage) {
    boolean answer = displayAlert("Exit", "Are you sure you want to exit?");
    if (answer) {
      stage.close();
      System.out.println("Program closed successfully.\nPlease come again!");
    }
  }
}
