package edu.ntnu.idatt2001.mappevurdering.ej.filehandling;

import edu.ntnu.idatt2001.mappevurdering.ej.models.Game;
import edu.ntnu.idatt2001.mappevurdering.ej.utils.GraphicalUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Class for writing files.
 */
public class WriteFile {
  private File file;

  /**
   * Saves a file.
   *
   * @param url the url of the file.
   * @param object the object to save.
   * @throws IOException if the file is not found.
   */
  private void saveFile(File url, Object object) throws IOException {

    if (!url.exists()) {
      boolean directory = url.getParentFile().mkdirs();
      boolean file = url.createNewFile();
      if (directory && file) {
        System.out.println("File was created");
        GraphicalUtils.getInstance().displayAlert("Error", "File was not created");
      }
    }

    ObjectOutputStream save = new ObjectOutputStream(new FileOutputStream(url));
    save.writeObject(object);
    save.close();
  }

  /**
   * Saves a game.
   *
   * @param game the game to save
   * @throws IOException if the file is not found
   */
  public void saveGame(Game game) throws IOException {
    file = new File("src/main/resources/1_Games/" + game.getTitle() + ".game");
    saveFile(file, game);
  }


  /**
   * Removes a game from the files.
   *
   * @param game the game to remove
   */
  public void removeGame(Game game) {
    file = new File("src/main/resources/1_Games/" + game.getTitle() + ".game");
    if (file.exists()) {
      boolean deleted = file.delete();
      if (deleted) {
        System.out.println("Game file deleted successfully.");
      } else {
        System.out.println("Failed to delete the game file.");
      }
    } else {
      System.out.println("Game file does not exist.");
    }
  }
}
