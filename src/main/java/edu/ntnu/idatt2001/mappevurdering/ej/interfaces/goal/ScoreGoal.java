package edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal;

import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import java.io.Serializable;


/**
 * Represents a goal that is fulfilled when the player has a certain amount of score.
 */
public class ScoreGoal implements Goal, Serializable {

  private final int scoreGoal;


  /**
   * Creates a new ScoreGoal.
   *
   * @param scoreGoal The amount of score to reach.
   */
  public ScoreGoal(int scoreGoal) {
    this.scoreGoal = scoreGoal;
  }


  /**
   * Checks if the goal is fulfilled.
   *
   * @param player the player to check.
   * @return true if the goal is fulfilled, false otherwise.
   */
  @Override
  public boolean isFulfilled(Player player) {
    return player.getScore() >= scoreGoal;
  }


  /**
   * Returns a string representation of the goal.
   *
   * @return a string representation of the goal.
   */
  @Override
  public String toString() {
    return "Score Goal: reach a score of '" + scoreGoal + "'";
  }
}
