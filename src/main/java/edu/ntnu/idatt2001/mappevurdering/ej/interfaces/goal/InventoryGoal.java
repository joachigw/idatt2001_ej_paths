package edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal;

import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import java.io.Serializable;


/**
 * Represents a goal that is fulfilled when the player has a certain amount of
 * items in their inventory, or when they have a certain item in their inventory.
 */
public class InventoryGoal implements Goal, Serializable {

  private int inventorySize;
  private String inventoryItem;


  /**
   * Creates a new InventoryGoal.
   *
   * @param goal the amount of items to reach.
   */
  public InventoryGoal(int goal) {
    this.inventorySize = goal;
  }


  /**
   * Creates a new InventoryGoal.
   *
   * @param goal the item to reach.
   */
  public InventoryGoal(String goal) {
    this.inventoryItem = goal;
  }


  /**
   * Checks if the goal is fulfilled.
   *
   * @param player the player to check.
   * @return true if the goal is fulfilled, false otherwise.
   */
  @Override
  public boolean isFulfilled(Player player) {
    if (inventorySize > 0) {
      return player.getInventory().size() >= inventorySize;
    } else {
      for (String item : player.getInventory()) {
        if (inventoryItem.equalsIgnoreCase(item)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Returns a string representation of the goal.
   *
   * @return a string representation of the goal.
   */
  @Override
  public String toString() {
    if (inventorySize > 0) {
      return "Inventory Goal: reach '" + inventorySize + "' items";
    } else {
      return "Inventory Goal: get the '" + inventoryItem + "'";
    }
  }

}
