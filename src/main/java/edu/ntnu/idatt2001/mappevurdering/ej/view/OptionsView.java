package edu.ntnu.idatt2001.mappevurdering.ej.view;

import edu.ntnu.idatt2001.mappevurdering.ej.controllers.OptionsController;
import edu.ntnu.idatt2001.mappevurdering.ej.enums.FontSize;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.FontObserver;
import edu.ntnu.idatt2001.mappevurdering.ej.utils.FontSettings;
import java.util.Objects;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 * Class for the view that displays the options menu.
 */
public class OptionsView implements FontObserver {

  private final Stage stage;
  private final OptionsController controller;
  private final FontSettings fontSettings = FontSettings.getInstance();

  // Nodes to be updates when font settings are changed
  private Label fontSize;
  private Label boldFont;


  /**
   * Constructor for the OptionsView class.
   *
   * @param stage the stage to display the view on
   * @param controller the controller for the view
   */
  public OptionsView(Stage stage, OptionsController controller) {
    this.stage = stage;
    this.controller = controller;
    fontSettings.attach(this);
  }


  /**
   *  Sets up the view.
   *  Creates the layout and displays it on the stage.
   */
  public void setup() {



    // Create return to main menu button
    Button returnButton = new Button("<- Return to main menu");
    returnButton.setOnMouseClicked(e -> controller.returnButtonClicked());

    // Create labels, sliders and checkboxes
    fontSize = new Label("Font size: ");
    boldFont = new Label("Bold font: ");
    ChoiceBox<FontSize> fontSizeChoiceBox = new ChoiceBox<>();


    // Add items to the font size choice box, set default value and add listener
    fontSizeChoiceBox.getItems().addAll(FontSize.SMALL, FontSize.MEDIUM, FontSize.LARGE);
    fontSizeChoiceBox.setValue(fontSettings.getFontSizeEnum());
    fontSizeChoiceBox.getSelectionModel().selectedItemProperty()
        .addListener((observable, oldValue, newValue) -> {
          fontSettings.setFontSize(newValue);
        });

    // Add listener to the bold font checkbox
    CheckBox boldFontCheckBox = new CheckBox();
    boldFontCheckBox.setSelected(fontSettings.getIsBold());
    boldFontCheckBox.selectedProperty().addListener((observableValue, oldVal, newVal) -> {
      fontSettings.setIsBold(newVal);
    });

    // Add functionality for operating the choice box and checkbox with the keyboard (UD)
    fontSizeChoiceBox.setOnKeyPressed(e -> {
      if (e.getCode() == KeyCode.ENTER) {
        fontSizeChoiceBox.show();
      }
    });
    boldFontCheckBox.setOnKeyPressed(e -> {
      if (e.getCode() == KeyCode.ENTER) {
        boldFontCheckBox.setSelected(!boldFontCheckBox.isSelected());
      }
    });

    // Create gridpane and add rows
    GridPane optionControls = new GridPane();
    optionControls.addRow(0, fontSize, fontSizeChoiceBox);
    optionControls.addRow(1, boldFont, boldFontCheckBox);

    Label title = new Label("Options");
    // Create parent nodes and add children
    VBox content = new VBox(title, optionControls);

    // Create MenuBar




    // Set alignments
    StackPane.setAlignment(returnButton, Pos.TOP_LEFT);
    StackPane.setAlignment(content, Pos.CENTER);

    // Set ids and add the stylesheet

    returnButton.setId("return-button");
    title.setId("title");
    fontSizeChoiceBox.setId("font-size-choice-box");
    boldFontCheckBox.setId("bold-font-checkbox");
    optionControls.setId("option-controls");
    content.setId("content-v-box");
    StackPane stackPane = new StackPane(content, returnButton);
    MenuBar menuBar = createMenuBar();
    VBox root = new VBox(menuBar, stackPane);
    root.setId("root-v-box");
    Scene scene = new Scene(root);
    scene.getStylesheets().add(Objects.requireNonNull(getClass()
        .getResource("/css/options-scene.css")).toExternalForm());

    // Ensure that the current font settings are applied
    updateFont();

    // Set scene and show stage
    stage.setTitle("Paths - Options");
    stage.setScene(scene);
    stage.show();
  }


  /**
   * Creates the menubar.
   *
   * @return the menubar
   */
  private MenuBar createMenuBar() {

    //Create menubar and menu items
    MenuItem helpMenuItem = new MenuItem("Help");
    helpMenuItem.setOnAction(e -> controller.showHelp());

    Menu helpMenu = new Menu("Show help", null, helpMenuItem);

    return new MenuBar(helpMenu);
  }


  /**
   * Updates the view based on changes in the font size setting.
   */
  @Override
  public void updateFont() {
    String fontStyle = "-fx-font-size: " + fontSettings.getFontSize()
        + "px; -fx-font-weight: " + fontSettings.getFontWeightString();

    fontSize.setStyle(fontStyle);
    boldFont.setStyle(fontStyle);
  }
}
