package edu.ntnu.idatt2001.mappevurdering.ej.models;

import java.io.File;
import java.io.Serializable;
import java.util.Map;

/**
 * Represents a story.
 */
public class Story implements Serializable {
  private File script;
  private final String title;
  private final Map<Link, Passage> passages;


  /**
   * Creates a new Story.
   *
   * @param title the title of the story
   *
   * @param passages the passages of the story
   */
  public Story(String title, Map<Link, Passage> passages) {
    this.title = title;
    this.passages = passages;
  }

  /**
   * Creates a new Story.
   *
   * @param title the title of the story
   *
   * @param passages the passages of the story
   *
   * @param script the script of the story
   */
  public Story(String title, Map<Link, Passage> passages, File script) {
    this.title = title;
    this.passages = passages;
    this.script = script;

  }

  /**
   * gets the story title.
   *
   * @return the story title
   */
  public String getTitle() {
    return title;
  }

  /**
   * gets the story passages.
   *
   * @return the story passages as a map
   */
  public Map<Link, Passage> getPassages() {
    return passages;
  }

  /**
   * gets a passage in the story using a link.
   *
   * @param link the link of the passage
   * @return the story passage
   */
  public Passage getPassage(Link link) {
    return passages.get(link);
  }

  /**
   * gets the story script.
   *
   * @return  script the story script
   */
  public File getScript() {
    return script;
  }

  /**
   * toString method for story.
   *
   * @return the story as a string
   */
  @Override
  public String toString() {
    return "Story{"
        + "title='" + title + '\''
        + ", passages=" + passages + '}';
  }
}



