package edu.ntnu.idatt2001.mappevurdering.ej.view;

import edu.ntnu.idatt2001.mappevurdering.ej.controllers.NewGameController;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.FontObserver;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.Goal;
import edu.ntnu.idatt2001.mappevurdering.ej.utils.FontSettings;
import java.io.IOException;
import java.util.Objects;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;


/**
 * Class for the view that displays the new game menu.
 */
public class NewGameView implements FontObserver {

  private final Stage stage;
  private final NewGameController controller;
  private final FontSettings fontSettings = FontSettings.getInstance();

  private final VBox player = new VBox();
  private final VBox story = new VBox();
  private final VBox goals = new VBox();
  private final GridPane gridBase = new GridPane();
  private final StackPane root = new StackPane();

  private TextField gameNameField;
  private TextField playerNameField;
  private TextField playerHealthField;
  private TextField playerGoldField;
  private TextField playerScoreField;
  private static ListView<String> pIList;
  private static Button storyChoice;
  private static ScrollPane storyContent;
  private ListView<Goal> goalList;
  private Label userFeedback;
  private Label linkAlert;
  private Button showBrokenLinksBtn;
  private Button returnButton;
  private Button nextB;
  private Label statName;
  private Label statGold;
  private Label statScore;
  private Label statInventory;
  private Label statHealth;
  private Label storyTitle;
  private Label aboutTitle;
  private Label goalTitle;
  private Button addGoal;
  private Label list;
  private Label playerTitle;
  private Button statInventoryAdd;

  /**
   * Constructor for NewGameView.
   *
   * @param stage the stage to display the view on
   *
   * @param controller the controller for the view
   */
  public NewGameView(Stage stage, NewGameController controller) {
    this.stage = stage;
    this.controller = controller;
  }

  /**
   * Sets up the view.
   * Creates the layout and displays it on the stage.
   */
  public void setup() {

    // Add the menu bar to the top of the window
    MenuBar menuBar = createMenuBar();
    StackPane.setAlignment(menuBar, Pos.TOP_LEFT);
    menuBar.setStyle("-fx-text-fill: black");

    root.setPrefHeight(Screen.getPrimary().getBounds().getHeight() * 0.8);
    root.setPrefWidth(Screen.getPrimary().getBounds().getWidth() * 0.8);
    root.setAlignment(Pos.CENTER);



    //first part of grid from top
    returnButton = new Button("<- Return to main menu");
    GridPane.setConstraints(returnButton, 0, 0, 1, 1, HPos.LEFT, VPos.CENTER);
    GridPane.setMargin(returnButton, new Insets(20));
    returnButton.setOnAction(e -> controller.returnButtonClicked());


    Label windowTitle = new Label("Create Game");
    GridPane.setConstraints(windowTitle, 0, 0, 3, 1, HPos.CENTER, VPos.BOTTOM);
    GridPane.setMargin(windowTitle, new Insets(10));


    gridBase.getChildren().addAll(returnButton, windowTitle);


    //second part of grid from top
    gameNameField = new TextField();
    gameNameField.setPromptText("Game Name");
    gameNameField.setStyle("-fx-alignment: center;");
    GridPane.setConstraints(gameNameField, 1, 1, 1, 1, HPos.CENTER, VPos.CENTER);
    GridPane.setMargin(gameNameField, new Insets(10, 50, 10, 50));
    gameNameField.setPrefSize(100, 50);


    gridBase.getChildren().add(gameNameField);


    //third part of grid from top
    playerInputArea();
    GridPane.setMargin(player, new Insets(10));
    player.prefWidthProperty().bind(gridBase.widthProperty().multiply(0.33));


    storyInputArea();
    GridPane.setMargin(story, new Insets(10));
    story.prefWidthProperty().bind(gridBase.widthProperty().multiply(0.33));


    goalsInputArea();
    GridPane.setMargin(goals, new Insets(10));
    goals.prefWidthProperty().bind(gridBase.widthProperty().multiply(0.33));

    //fourth part of grid from top
    userFeedback = new Label("");
    GridPane.setConstraints(userFeedback, 0, 4, 3, 2, HPos.CENTER, VPos.CENTER);
    GridPane.setMargin(userFeedback, new Insets(0, 10, 10, 10));
    userFeedback.setStyle("-fx-text-fill: green;");

    //fifth part of grid from top
    nextB = new Button("NEXT");
    GridPane.setConstraints(nextB, 1, 5, 1, 1, HPos.CENTER, VPos.CENTER);
    GridPane.setMargin(nextB, new Insets(10, 10, 30, 10));
    nextB.setOnAction(e -> {
      try {
        controller.assign_NextB_Action(nextB);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });


    gridBase.getChildren().addAll(userFeedback, nextB);
    gridBase.getStyleClass().add("gridpane-with-border");


    root.getChildren().addAll(gridBase, menuBar);


    gridBase.setPrefHeight(root.getPrefHeight());
    gridBase.setPrefWidth(root.getPrefWidth());
    gridBase.setAlignment(Pos.CENTER);




    //Set css-ids for the nodes and add the stylesheet to this scene
    returnButton.setId("return-button");
    nextB.setId("next-button");
    userFeedback.setId("user-feedback");
    windowTitle.setId("title");
    gameNameField.setId("game-name-text-field");
    player.setId("player-container");
    statInventoryAdd.setId("add-inventory-button");
    story.setId("story-container");
    storyChoice.setId("story-choice-button");
    goals.setId("goals-container");
    addGoal.setId("add-goal-button");
    root.setId("anchor-pane");

    // Ensure that the current font settings are applied
    updateFont();

    stage.setTitle("Paths - New Game");
    Scene scene = new Scene(root);
    scene.getStylesheets().add(Objects.requireNonNull(getClass()
        .getResource("/css/new-game-scene.css")).toExternalForm());

    stage.setScene(scene);
    stage.show();
  }


  /**
   * Creates the player input area.
   * Here the user can enter the name, health, gold and strength of the player.
   */
  private void playerInputArea() {
    HBox playerBoxTitle = new HBox();
    playerTitle = new Label("PLAYER");
    playerBoxTitle.getChildren().add(playerTitle);
    playerBoxTitle.setAlignment(Pos.CENTER);

    GridPane playerGrid = new GridPane();
    playerGrid.setAlignment(Pos.CENTER);
    playerGrid.setPadding(new Insets(15));

    //Name part of the player box
    statName = new Label("Name: ");
    GridPane.setConstraints(statName, 0, 0, 1, 1, HPos.LEFT, VPos.CENTER);
    playerNameField = new TextField();
    playerNameField.setPromptText("Character Name");
    GridPane.setConstraints(playerNameField, 1, 0, 1, 1, HPos.RIGHT, VPos.CENTER);
    GridPane.setMargin(playerNameField, new Insets(10, 10, 10, 25));
    playerGrid.getChildren().addAll(statName, playerNameField);

    //Health part of the player box
    statHealth = new Label("Health: ");
    GridPane.setConstraints(statHealth, 0, 1, 1, 1, HPos.LEFT, VPos.CENTER);
    playerHealthField = new TextField();
    playerHealthField.setPromptText("(optional)");
    GridPane.setConstraints(playerHealthField, 1, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
    GridPane.setMargin(playerHealthField, new Insets(10, 10, 10, 25));
    playerGrid.getChildren().addAll(statHealth, playerHealthField);

    //Gold part of the player box
    statGold = new Label("Gold: ");
    GridPane.setConstraints(statGold, 0, 2, 1, 1, HPos.LEFT, VPos.CENTER);
    playerGoldField = new TextField();
    playerGoldField.setPromptText("(optional)");
    GridPane.setConstraints(playerGoldField, 1, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
    GridPane.setMargin(playerGoldField, new Insets(10, 10, 10, 25));
    playerGrid.getChildren().addAll(statGold, playerGoldField);

    //Score part of the player box
    statScore = new Label("Score: ");
    GridPane.setConstraints(statScore, 0, 3, 1, 1, HPos.LEFT, VPos.CENTER);
    playerScoreField = new TextField();
    playerScoreField.setPromptText("(optional)");
    GridPane.setConstraints(playerScoreField, 1, 3, 1, 1, HPos.RIGHT, VPos.CENTER);
    GridPane.setMargin(playerScoreField, new Insets(10, 10, 10, 25));
    playerGrid.getChildren().addAll(statScore, playerScoreField);


    //Inventory part of the player box
    statInventory = new Label("Inventory: ");
    GridPane.setConstraints(statInventory, 0, 4, 1, 1, HPos.LEFT, VPos.CENTER);
    statInventoryAdd = new Button("Add item");
    statInventoryAdd.setOnAction(e -> controller.assignPlayerInventoryAdd_Action());

    GridPane.setConstraints(statInventoryAdd, 1, 4, 1, 1, HPos.RIGHT, VPos.CENTER);
    GridPane.setMargin(statInventoryAdd, new Insets(25, 10, 10, 25));
    pIList = new ListView<>();
    MenuItem delete = new CheckMenuItem("Remove");
    pIList.setContextMenu(new ContextMenu(delete));
    delete.setOnAction(e -> controller.deleteInventoryItemClick());
    ScrollPane statInventoryScroll = new ScrollPane(pIList);
    statInventoryScroll.setFitToWidth(true);
    GridPane.setConstraints(statInventoryScroll, 0, 5, 2, 1, HPos.CENTER, VPos.CENTER);
    playerGrid.getChildren().addAll(statInventory, statInventoryAdd, statInventoryScroll);


    //Finishing and adding all parts to player box
    player.getChildren().addAll(playerBoxTitle, playerGrid);
    player.setPadding(new Insets(10, 10, 30, 10));
    GridPane.setConstraints(player, 0, 2, 1, 1, HPos.CENTER, VPos.CENTER);
    gridBase.getChildren().add(player);

  }

  /**
   * Creates the story input area.
   * Here the user can choose a story from a file.
   */
  private void storyInputArea() {
    HBox storyBoxTitle = new HBox();
    storyTitle = new Label("STORY");
    storyBoxTitle.getChildren().add(storyTitle);
    storyBoxTitle.setAlignment(Pos.CENTER);


    GridPane storyGrid = new GridPane();
    storyGrid.setAlignment(Pos.CENTER);


    storyChoice = new Button("Choose a Story from File");
    storyChoice.setOnAction(e -> {
      try {
        controller.assignStoryAdd_Action();
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });


    GridPane.setConstraints(storyChoice, 0, 0, 2, 1, HPos.CENTER, VPos.CENTER);


    storyGrid.getChildren().add(storyChoice);

    //Content window that shows the story description (first passage content?)
    aboutTitle = new Label("About:");
    GridPane.setConstraints(aboutTitle, 0, 1, 1, 1, HPos.LEFT, VPos.CENTER);
    GridPane.setMargin(aboutTitle, new Insets(20, 0, 5, 0));
    storyContent = new ScrollPane();
    storyContent.setFitToWidth(true);
    storyContent.prefWidthProperty().bind(pIList.widthProperty());
    storyContent.prefHeightProperty().bind(pIList.heightProperty());
    GridPane.setConstraints(storyContent, 0, 2, 2, 1);
    storyGrid.getChildren().addAll(aboutTitle, storyContent);



    //Alert part of Story
    linkAlert = new Label("");
    showBrokenLinksBtn = new Button("SHOW");
    showBrokenLinksBtn.setPrefSize(100, 40);
    showBrokenLinksBtn.setVisible(false);
    showBrokenLinksBtn.setPadding(new Insets(0, 0, 0, 0));
    HBox linkAlertBox = new HBox(linkAlert);
    HBox showBrokenLinksBox = new HBox(showBrokenLinksBtn);
    linkAlertBox.setAlignment(Pos.CENTER);
    showBrokenLinksBox.setAlignment(Pos.CENTER);
    VBox brokenLinksBox = new VBox(linkAlertBox, showBrokenLinksBox);
    brokenLinksBox.setAlignment(Pos.CENTER);
    brokenLinksBox.setSpacing(20);

    GridPane.setConstraints(brokenLinksBox, 0, 3, 1, 1, HPos.CENTER, VPos.CENTER);
    GridPane.setMargin(brokenLinksBox, new Insets(20, 0, 0, 0));
    ImageView helpIcon = new ImageView("file:pictures/ICONS/helpIcon.png");
    helpIcon.setFitHeight(30);
    helpIcon.setFitWidth(50);
    GridPane.setConstraints(helpIcon, 1, 3, 1, 1, HPos.RIGHT, VPos.CENTER);
    GridPane.setMargin(helpIcon, new Insets(20, 0, 0, 0));
    storyGrid.getChildren().addAll(brokenLinksBox);


    story.getChildren().addAll(storyBoxTitle, storyGrid);
    story.setPadding(new Insets(10, 10, 30, 10));
    GridPane.setConstraints(story, 1, 2, 1, 1, HPos.CENTER, VPos.CENTER);
    gridBase.getChildren().add(story);
  }

  /**
   * Creates the goals input area.
   * Here the user can add goals to the story.
   */
  private void goalsInputArea() {
    HBox goalsBoxTitle = new HBox();
    goalTitle = new Label("GOALS");
    goalsBoxTitle.getChildren().add(goalTitle);
    goalsBoxTitle.setAlignment(Pos.CENTER);

    GridPane goalsGrid = new GridPane();
    goalsGrid.setAlignment(Pos.CENTER);
    goalsGrid.setPadding(new Insets(15));

    //Button to add new goal
    addGoal = new Button("Add goal");
    addGoal.setOnAction(e -> controller.assignGoalsAdd_Action());
    GridPane.setConstraints(addGoal, 0, 0, 2, 1, HPos.CENTER, VPos.CENTER);
    goalsGrid.getChildren().add(addGoal);

    //Goal list
    list = new Label("List:");
    GridPane.setConstraints(list, 0, 1, 1, 1, HPos.LEFT, VPos.CENTER);
    GridPane.setMargin(list, new Insets(20, 0, 5, 0));
    goalList = new ListView<>();
    MenuItem delete = new MenuItem("Remove");
    goalList.setContextMenu(new ContextMenu(delete));
    delete.setOnAction(e -> controller.deleteGoalClick());
    ScrollPane goalScroll = new ScrollPane(goalList);
    goalList.prefWidthProperty().bind(pIList.widthProperty());
    goalList.prefHeightProperty().bind(pIList.heightProperty());
    goalScroll.setFitToWidth(true);
    GridPane.setConstraints(goalScroll, 0, 2, 2, 1);
    goalsGrid.getChildren().addAll(list, goalScroll);

    goals.getChildren().addAll(goalsBoxTitle, goalsGrid);
    goals.setPadding(new Insets(10, 10, 30, 10));
    GridPane.setConstraints(goals, 2, 2, 1, 1, HPos.CENTER, VPos.CENTER);
    gridBase.getChildren().add(goals);
  }

  /**
   * returns the label that displays the user feedback when file is successfully created.
   *
   * @return Label userFeedback
   */
  public Label getUserFeedbackLabel() {
    return userFeedback;
  }

  /**
   * returns the TextField where you can enter the name of the game.
   *
   * @return TextField gameNameField
   */
  public TextField getGameNameField() {
    return gameNameField;
  }

  /**
   * returns the TextField where you can enter the name of player.
   *
   * @return TextField pNField
   */
  public TextField getPlayerNameField() {
    return playerNameField;
  }

  /**
   * returns the TextField where you can enter the value of player health.
   *
   * @return TextField pHField
   */
  public TextField getPlayerHealthField() {
    return playerHealthField;
  }

  /**
   * returns the TextField where you can enter the value of player gold.
   *
   * @return TextField pGField
   */
  public TextField getPlayerGoldField() {
    return playerGoldField;
  }

  /**
   * returns the TextField where you can enter the value of player score.
   *
   * @return TextField pSField
   */
  public TextField getPlayerScoreField() {
    return playerScoreField;
  }

  /**
   * returns the ListView that contains the player inventory.
   *
   * @return TextField pIField
   */
  public ListView<String> getPlayerInventoryList() {
    return pIList;
  }

  /**
   * returns the button that opens file explorer to select a script for the game.
   *
   * @return Button storyChoice
   */
  public Button getStoryChoice() {
    return storyChoice;
  }

  /**
   * returns the scroll pane that contains the story description.
   *
   * @return ScrollPane storyContent
   */
  public ScrollPane getStoryContent() {
    return storyContent;
  }

  /**
   * returns the ListView that contains the goals.
   *
   * @return ListView goalList
   */
  public ListView<Goal> getGoalList() {
    return goalList;
  }

  /**
   * returns the label that can tells the user about broken links.
   *
   * @return Label link_alert
   */
  public Label getLinkAlert() {
    return linkAlert;
  }

  /**
   * returns the button that can show the broken links.
   *
   * @return Button link_Btn
   */
  public Button getLinkBtn() {
    return showBrokenLinksBtn;
  }


  /**
   * Creates the menu bar for the view.
   *
   * @return the menu bar
   */
  private MenuBar createMenuBar() {

    //Create menubar and menu items
    MenuItem helpMenuItem = new MenuItem("Help");
    helpMenuItem.setOnAction(e -> controller.showHelp());

    Menu helpMenu = new Menu("Show help", null, helpMenuItem);

    return new MenuBar(helpMenu);
  }

  /**
   * Updates the view based on changes in the font size setting.
   */
  @Override
  public void updateFont() {
    String fontStyle = "-fx-font-size: " + fontSettings.getFontSize()
        + "px; -fx-font-weight: "
        + fontSettings.getFontWeightString();

    storyChoice.setStyle(fontStyle);
    storyContent.setStyle(fontStyle);
    goalList.setStyle(fontStyle);
    userFeedback.setStyle(fontStyle);
    linkAlert.setStyle(fontStyle);
    showBrokenLinksBtn.setStyle(fontStyle);
    returnButton.setStyle(fontStyle);
    nextB.setStyle(fontStyle);
    statName.setStyle(fontStyle);
    statGold.setStyle(fontStyle);
    statScore.setStyle(fontStyle);
    statInventory.setStyle(fontStyle);
    statHealth.setStyle(fontStyle);
    storyTitle.setStyle(fontStyle);
    aboutTitle.setStyle(fontStyle);
    goalTitle.setStyle(fontStyle);
    addGoal.setStyle(fontStyle);
    list.setStyle(fontStyle);
    playerTitle.setStyle(fontStyle);
    statInventoryAdd.setStyle(fontStyle);
  }
}
