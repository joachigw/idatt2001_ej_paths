package edu.ntnu.idatt2001.mappevurdering.ej.interfaceTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.GoldAction;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.GoldGoal;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

/**
 * Test class for the GoldAction and GoldGoal classes.
 */
public class GoldTest {

  @Test
  @DisplayName("Gold Action Test")
  public void testGoldAction() {
    // Create a player with initial gold of 50
    Player player = new Player.Builder("Test Player").gold(50).build();

    // Create a GoldAction with a positive value
    GoldAction goldAction = new GoldAction(55);
    // Execute the action on the player
    goldAction.execute(player);
    // Check if the player's gold has increased from 50 to 105
    assertEquals(105, player.getGold());

    // Create a GoldAction with a negative value
    goldAction = new GoldAction(-20);
    // Execute the action on the player
    goldAction.execute(player);
    // Check if the player's gold has decreased from 105 to 85
    assertEquals(85, player.getGold());
  }

  @Test
  @DisplayName("Gold Action toString Test")
  public void testActionToString() {
    // Create a GoldAction with a negative gold value
    GoldAction goldAction = new GoldAction(-50);

    // Get the string representation of the action
    String actionString = goldAction.toString();

    // Check if the string representation is correct
    assertEquals("You lost: 50 gold...", actionString);
  }

  @Test
  @DisplayName("Gold Goal is Fulfilled")
  public void testGoldIsFulfilled() {
    // Create a player with initial gold of 150
    Player player = new Player.Builder("Test Player").gold(150).build();

    // Create a GoldGoal with a gold goal of 100
    GoldGoal goldGoal = new GoldGoal(100);

    // Check if the gold goal is fulfilled for the player
    assertTrue(goldGoal.isFulfilled(player));
  }

  @Test
  @DisplayName("Gold Goal toString Test")
  public void testGoalToString() {
    // Create a GoldGoal with a gold goal of 200
    GoldGoal goldGoal = new GoldGoal(200);

    // Get the string representation of the goal
    String goalString = goldGoal.toString();

    // Check if the string representation is correct
    assertEquals("Gold Goal: reach '200' gold.", goalString);
  }

}
