package edu.ntnu.idatt2001.mappevurdering.ej.modelTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;




/**
 * Test class for the Player class.
 */
public class PlayerTest {

  @Test
  @DisplayName("Test Player Builder")
  public void testPlayerBuilder() {
    // Create a player using the builder pattern
    Player player = new Player.Builder("John")
        .health(100)
        .score(0)
        .gold(0)
        .inventory(new ArrayList<>())
        .build();

    // Verify player attributes
    assertEquals("John", player.getName());
    assertEquals(100, player.getHealth());
    assertEquals(0, player.getScore());
    assertEquals(0, player.getGold());
    assertNotNull(player.getInventory());
    assertTrue(player.getInventory().isEmpty());
  }

  @Test
  @DisplayName("Test Player Health")
  public void testPlayerHealth() {
    // Create a player
    Player player = new Player.Builder("John").build();

    // Verify initial health
    assertEquals(100, player.getHealth());

    // Add positive health
    player.addHealth(50);

    // Verify updated health
    assertEquals(150, player.getHealth());

    // Add negative health
    player.addHealth(-300);

    // Verify updated health (should be capped at 0)
    assertEquals(0, player.getHealth());
  }

  @Test
  @DisplayName("Test Player Score")
  public void testPlayerScore() {
    // Create a player
    Player player = new Player.Builder("John").build();

    // Verify initial score
    assertEquals(0, player.getScore());

    // Add positive score
    player.addScore(50);

    // Verify updated score
    assertEquals(50, player.getScore());

    // Add negative score
    player.addScore(-30);

    // Verify updated score
    assertEquals(20, player.getScore());
  }

  @Test
  @DisplayName("Test Player Gold")
  public void testPlayerGold() {
    // Create a player
    Player player = new Player.Builder("John").build();

    // Verify initial gold
    assertEquals(0, player.getGold());

    // Add positive gold
    player.addGold(50);

    // Verify updated gold
    assertEquals(50, player.getGold());

    // Add negative gold
    player.addGold(-30);

    // Verify updated gold (should be capped at 0)
    assertEquals(20, player.getGold());
  }

  @Test
  @DisplayName("Test Player Inventory")
  public void testPlayerInventory() {
    // Create a player
    Player player = new Player.Builder("John").build();

    // Verify initial inventory
    assertNotNull(player.getInventory());
    assertTrue(player.getInventory().isEmpty());

    // Add items to the inventory
    player.addToInventory("Sword");
    player.addToInventory("Shield");
    player.addToInventory("Potion");

    // Verify updated inventory
    List<String> expectedInventory = List.of("Sword", "Shield", "Potion");
    assertEquals(expectedInventory, player.getInventory());
  }
}
