package edu.ntnu.idatt2001.mappevurdering.ej.fileHandlingTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2001.mappevurdering.ej.CreateTestObjects;
import edu.ntnu.idatt2001.mappevurdering.ej.filehandling.ReadFile;
import edu.ntnu.idatt2001.mappevurdering.ej.filehandling.WriteFile;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Game;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Passage;
import java.io.File;
import java.io.IOException;
import org.junit.jupiter.api.Test;


/**
 * Test class for the ReadFile class.
 */
public class ReadFileTest {
  private final ReadFile readFile = new ReadFile();
  private final CreateTestObjects create = new CreateTestObjects();

  @Test
  public void testReadGame() throws IOException {
    // Create a test game object and save it to a file
    Game game = create.createTestGame();
    File file = new File("src/main/resources/1_Games/test.game");
    try {
      WriteFile writeFile = new WriteFile();
      writeFile.saveGame(game);
      // Read the game from the file
      Game loadedGame = readFile.readGame(file);
      // Verify that the loaded game is not null and has the expected properties
      assertNotNull(loadedGame);
      assertEquals(game.getTitle(), loadedGame.getTitle());
      // Add more assertions to compare other properties of the game object
    } catch (IOException e) {
      fail("IOException occurred: " + e.getMessage());
    } finally {
      // Clean up: delete the test file
      if (file.exists()) {
        boolean deleted = file.delete();
        if (deleted) {
          System.out.println("Game file deleted successfully.");
        } else {
          System.out.println("Failed to delete the game file.");
        }
      }
    }
  }

  @Test
  public void testReadPassage() {
    // Create a test passage in a script file
    File scriptFile = new File("src/test/java/edu/ntnu/idatt2001/"
            + "mappevurdering/ej/TestFiles/thevalleyofadventures.paths");
    String chosen = "First Passage";
    try {
      // Read the passage from the script file
      Passage passage = readFile.readPassage(chosen, scriptFile);

      // Verify that the loaded passage is not null and has the expected properties
      assertNotNull(passage);
      //Gets the first passage of the file which is "Opening Passage"
      assertEquals("Opening Passage", passage.getTitle());

    } catch (IOException e) {
      fail("Exception occurred: " + e.getMessage());
    }
  }

  @Test
  public void testGetBrokenLinks() {
    // Create a test script file with broken links
    File scriptFile = new File("src/test/java/edu/ntnu/idatt2001/"
            + "mappevurdering/ej/TestFiles/thevalleyofadventures.paths");
    // Get the broken links from the script file
    String brokenLinks = readFile.getBrokenLinks(scriptFile);
    // Verify that the broken links string is not null and contains the expected broken links
    assertNotNull(brokenLinks);
    assertTrue(brokenLinks.contains("Broken Passage"));
    assertTrue(brokenLinks.contains("Nowhere"));
    // Add more assertions to verify the format
    System.out.println(brokenLinks);
  }
}