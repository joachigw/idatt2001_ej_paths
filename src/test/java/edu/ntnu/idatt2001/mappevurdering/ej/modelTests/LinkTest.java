package edu.ntnu.idatt2001.mappevurdering.ej.modelTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.Action;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.HealthAction;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Link;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;


/**
 * Test class for the Link class.
 */
public class LinkTest {

  @Test
  @DisplayName("Test Link Getters")
  public void testLinkGetters() {
    // Create a Link object
    String text = "Travel";
    String reference = "Oslo";
    List<Action> actions = new ArrayList<>();
    Link link = new Link(text, reference, actions);

    // Test the getters
    assertEquals(text, link.getText());
    assertEquals(reference, link.getReference());
    assertEquals(actions, link.getActions());
  }

  @Test
  @DisplayName("Test Link Add Action")
  public void testLinkAddAction() {
    // Create a Link object
    Link link = new Link("Travel", "Oslo", new ArrayList<>());

    // Create an Action object
    Action action = new HealthAction(50);

    // Add the action to the link
    boolean added = link.addAction(action);

    // Verify that the action is added successfully
    assertTrue(added);
    assertTrue(link.getActions().contains(action));
  }

  @Test
  @DisplayName("Test Link String Representation")
  public void testLinkToString() {
    // Create a Link object
    Link link = new Link("Travel", "Oslo", new ArrayList<>());

    // Get the string representation of the link
    String linkString = link.toString();

    // Verify that the string representation is correct
    assertEquals("Travel", linkString);
  }

  @Test
  @DisplayName("Test Link Equality")
  public void testLinkEquality() {
    // Create two Link objects with the same attributes
    Link link1 = new Link("Travel", "Oslo", new ArrayList<>());
    Link link2 = new Link("Travel", "Oslo", new ArrayList<>());

    // Verify that the links are equal
    assertEquals(link1, link2);
  }

  @Test
  @DisplayName("Test Link Hash Code")
  public void testLinkHashCode() {
    // Create a Link object
    Link link = new Link("Travel", "Oslo", new ArrayList<>());

    // Get the hash code of the link
    int hashCode = link.hashCode();

    // Verify that the hash code is not zero
    assertTrue(hashCode != 0);
  }

  @Test
  @DisplayName("Test Link File String Representation")
  public void testLinkToFileString() {
    // Create a Link object
    Link link = new Link("Travel", "Oslo", new ArrayList<>());

    // Get the file string representation of the link
    String fileString = link.toFileString();

    // Verify that the file string representation is correct
    assertEquals("[Travel] (Oslo) {}", fileString);
  }
}
