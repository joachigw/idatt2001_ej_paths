package edu.ntnu.idatt2001.mappevurdering.ej.modelTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import edu.ntnu.idatt2001.mappevurdering.ej.models.Link;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Passage;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Story;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Test class for the Story class.
 */
public class StoryTest {

  @Test
  @DisplayName("Test Story Creation and Getters")
  public void testStoryCreation() {
    // Create a test passage
    List<Link> links = new ArrayList<>();
    links.add(new Link("Link 1", "Reference 1", new ArrayList<>()));
    Passage passage = new Passage("Passage 1", "Content 1", links);

    // Create a test story
    Map<Link, Passage> passages = new HashMap<>();
    passages.put(links.get(0), passage);
    Story story = new Story("Test Story", passages, new File("script.txt"));

    // Test story creation
    assertNotNull(story);
    assertEquals("Test Story", story.getTitle());
    assertEquals(passages, story.getPassages());
    assertEquals(new File("script.txt"), story.getScript());
  }

  @Test
  @DisplayName("Test Story GetPassage")
  public void testStoryGetPassage() {
    // Create passages for the story
    Passage passage1 = new Passage("Passage 1", "Content 1", null);
    Passage passage2 = new Passage("Passage 2", "Content 2", null);

    // Create links for the passages
    Link link1 = new Link("Link 1", "Reference 1", new ArrayList<>());
    Link link2 = new Link("Link 2", "Reference 2", new ArrayList<>());

    // Create map of passages and links
    Map<Link, Passage> passages = new HashMap<>();
    passages.put(link1, passage1);
    passages.put(link2, passage2);

    // Create a story with the passages
    Story story = new Story("My Story", passages);

    // Verify the retrieved passages using the links
    assertEquals(passage1, story.getPassage(link1));
    assertEquals(passage2, story.getPassage(link2));
  }

  @Test
  @DisplayName("Test Story RemovePassage")
  public void testStoryRemovePassage() {
    // Create links for the passages
    Link link1 = new Link("Link 1", "Reference 1", new ArrayList<>());
    Link link2 = new Link("Link 2", "Reference 2", new ArrayList<>());

    // Create passages for the story with the respective links
    Passage passage1 = new Passage("Passage 1", "Content 1",
            List.of(new Link(link1.getText(), link1.getReference(), new ArrayList<>())));
    Passage passage2 = new Passage("Passage 2", "Content 2",
            List.of(new Link(link2.getText(), link2.getReference(), new ArrayList<>())));

    // Create map of passages and links
    Map<Link, Passage> passages = new HashMap<>();
    passages.put(link1, passage1);
    passages.put(link2, passage2);

    // Create a story with the passages
    Story story = new Story("My Story", passages);

    // Verify the initial number of passages
    assertEquals(2, story.getPassages().size());

    // Try to remove a passage that is not referenced by any other passage
    assertNotNull(story.getPassage(link1)); // Verify that the passage exists before removal
    story.getPassages().remove(link1);
    assertNull(story.getPassage(link1)); // Verify that the passage has been removed
    assertEquals(1, story.getPassages().size());

    // Try to remove a passage that is referenced by another passage
    assertNotNull(story.getPassage(link2)); // Verify that the passage exists before removal
    story.getPassages().remove(link2);
    assertNull(story.getPassage(link2)); // Verify that the passage has not been removed
    assertEquals(0, story.getPassages().size());
  }

  @Test
  @DisplayName("Test Story toString")
  public void testStoryToString() {
    // Create a test passage
    List<Link> links = new ArrayList<>();
    links.add(new Link("Link 1", "Reference 1", new ArrayList<>()));
    Passage passage = new Passage("Passage 1", "Content 1", links);

    // Create a test story
    Map<Link, Passage> passages = new HashMap<>();
    passages.put(links.get(0), passage);
    Story story = new Story("Test Story", passages, new File("script.txt"));

    // Verify the toString method
    assertEquals("Story{title='Test Story', passages={Link 1=Passage{title='Passage 1', "
            + "content='Content 1', links=[Link 1]}\n}}", story.toString());
  }
}
