package edu.ntnu.idatt2001.mappevurdering.ej.interfaceTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.ScoreAction;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.ScoreGoal;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;


/**
 * Test class for the ScoreAction and ScoreGoal classes.
 */
public class ScoreTest {
  @Test
  @DisplayName("Score Action Test")
  public void testScoreAction() {
    // Create a player with initial score of 100
    Player player = new Player.Builder("Test Player").score(100).build();

    // Create a ScoreAction with a positive value
    ScoreAction scoreAction = new ScoreAction(50);
    // Execute the action on the player
    scoreAction.execute(player);
    // Check if the player's score has increased from 100 to 150
    assertEquals(150, player.getScore());

    // Create a ScoreAction with a negative value
    scoreAction = new ScoreAction(-20);
    // Execute the action on the player
    scoreAction.execute(player);
    // Check if the player's score has decreased from 150 to 130
    assertEquals(130, player.getScore());
  }

  @Test
  @DisplayName("Score Action toString Test")
  public void testActionToString() {
    // Create a ScoreAction with a negative score value
    ScoreAction scoreAction = new ScoreAction(-50);

    // Get the string representation of the action
    String actionString = scoreAction.toString();

    // Check if the string representation is correct
    assertEquals("You lost: 50 score...", actionString);
  }

  @Test
  @DisplayName("Score Goal is Fulfilled")
  public void testScoreIsFulfilled() {
    // Create a ScoreGoal with a score goal of 200
    ScoreGoal scoreGoal = new ScoreGoal(200);

    // Create a Player with a score value of 250
    Player player = new Player.Builder("Test Player").score(250).build();

    // Check if the score goal is fulfilled for the player
    assertTrue(scoreGoal.isFulfilled(player));
  }

  @Test
  @DisplayName("Score Goal toString Test")
  public void testGoalToString() {
    // Create a ScoreGoal with a score goal of 500
    ScoreGoal scoreGoal = new ScoreGoal(500);

    // Get the string representation of the goal
    String goalString = scoreGoal.toString();

    // Check if the string representation is correct
    assertEquals("Score Goal: reach a score of '500'", goalString);
  }
}
