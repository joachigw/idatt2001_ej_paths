package edu.ntnu.idatt2001.mappevurdering.ej.fileHandlingTests;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2001.mappevurdering.ej.CreateTestObjects;
import edu.ntnu.idatt2001.mappevurdering.ej.filehandling.WriteFile;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Game;
import java.io.File;
import java.io.IOException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;



/**
 * Test class for the WriteFile class.
 */
public class WriteFileTest {
  private static final String TEST_DIRECTORY = "src/main/resources/1_Games/";
  private static final String TEST_GAME_TITLE = "test";

  private final WriteFile writeFile = new WriteFile();
  private final CreateTestObjects create = new CreateTestObjects();

  @AfterEach
  void cleanup() {
    // Clean up: delete the test files
    File gameFile = new File(TEST_DIRECTORY  + TEST_GAME_TITLE + ".game");

    if (gameFile.exists()) {
      boolean deleted = gameFile.delete();
      if (deleted) {
        System.out.println("Game file deleted successfully.");
      } else {
        System.out.println("Failed to delete the game file.");
      }
    }
  }

  @Test
  void testSaveGame() throws IOException {

    // Create a test game
    Game testGame = create.createTestGame();

    try {
      // Save the test game
      writeFile.saveGame(testGame);

      // Check if the game file exists
      File gameFile = new File(TEST_DIRECTORY + TEST_GAME_TITLE + ".game");
      assertTrue(gameFile.exists());
    } catch (IOException e) {
      fail("IOException occurred: " + e.getMessage());
    }
  }

  @Test
  void testRemoveGame() throws IOException {

    // Create a test game
    Game testGame = create.createTestGame();

    try {
      // Save the test game
      writeFile.saveGame(testGame);

      // Remove the game
      writeFile.removeGame(testGame);

      // Check if the game file is deleted
      File gameFile = new File(TEST_DIRECTORY + TEST_GAME_TITLE + ".game");
      assertFalse(gameFile.exists());
    } catch (IOException e) {
      fail("IOException occurred: " + e.getMessage());
    }
  }

}
