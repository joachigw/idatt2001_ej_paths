package edu.ntnu.idatt2001.mappevurdering.ej.interfaceTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.InventoryAction;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.InventoryGoal;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import java.util.ArrayList;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;


/**
 * Test class for the InventoryAction and InventoryGoal classes.
 */
public class InventoryTest {

  @Test
  @DisplayName("Inventory Action Test")
  public void testInventoryAction() {
    // Create a player with an empty inventory
    Player player = new Player.Builder("Test Player").build();

    // Create an InventoryAction with an item
    InventoryAction inventoryAction = new InventoryAction("Sword");
    // Execute the action on the player
    inventoryAction.execute(player);
    // Check if the player's inventory contains the item
    assertTrue(player.getInventory().contains("Sword"));

    // Create an InventoryAction with another item
    inventoryAction = new InventoryAction("Shield");
    // Execute the action on the player
    inventoryAction.execute(player);
    // Check if the player's inventory contains both items
    assertTrue(player.getInventory().contains("Sword"));
    assertTrue(player.getInventory().contains("Shield"));
  }

  @Test
  @DisplayName("Inventory Action toString Test")
  public void testActionToString() {
    // Create an InventoryAction with an item
    InventoryAction inventoryAction = new InventoryAction("Potion");

    // Get the string representation of the action
    String actionString = inventoryAction.toString();

    // Check if the string representation is correct
    assertEquals("You got a new item: 'Potion' acquired!", actionString);
  }

  @Test
  @DisplayName("Inventory Goal is Fulfilled")
  public void testInventoryIsFulfilled() {
    // Create a player with an inventory containing the required item
    ArrayList<String> items = new ArrayList<String>();
    items.add("Sword");
    items.add("Shield");
    Player player = new Player.Builder("Test Player")
        .inventory(items)
        .build();

    // Create an InventoryGoal with the required item
    InventoryGoal inventoryGoal = new InventoryGoal("Sword");

    // Check if the inventory goal is fulfilled for the player
    assertTrue(inventoryGoal.isFulfilled(player));
  }

  @Test
  @DisplayName("Inventory Goal toString Test")
  public void testGoalToString() {
    // Create an InventoryGoal with a required item
    InventoryGoal inventoryGoal = new InventoryGoal("Key");

    // Get the string representation of the goal
    String goalString = inventoryGoal.toString();

    // Check if the string representation is correct
    assertEquals("Inventory Goal: get the 'Key'", goalString);
  }
}
