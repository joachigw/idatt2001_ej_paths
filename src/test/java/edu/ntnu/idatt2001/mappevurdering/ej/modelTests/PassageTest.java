package edu.ntnu.idatt2001.mappevurdering.ej.modelTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.Action;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.GoldAction;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.InventoryAction;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Link;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Passage;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;




/**
 * Test class for the Passage class.
 */
public class PassageTest {


  @Test
  @DisplayName("Test Passage Title")
  public void testPassageTitle() {
    // Create a passage with a title
    Passage passage = new Passage("Title", "Content", new ArrayList<>());

    // Verify the passage title
    assertEquals("Title", passage.getTitle());

    // Update the passage title
    passage.setTitle("New Title");

    // Verify the updated passage title
    assertEquals("New Title", passage.getTitle());
  }

  @Test
  @DisplayName("Test Passage Content")
  public void testPassageContent() {
    // Create a passage with content
    Passage passage = new Passage("Title", "Content", new ArrayList<>());

    // Verify the passage content
    assertEquals("Content", passage.getContent());

    // Update the passage content
    passage.setContent("New Content");

    // Verify the updated passage content
    assertEquals("New Content", passage.getContent());
  }

  @Test
  @DisplayName("Test Passage with Links")
  public void testPassageWithLinks() {
    // Create actions
    Action action1 = new InventoryAction("Item 1");
    Action action2 = new GoldAction(100);

    // Create links with actions
    Link link1 = new Link("Travel", "City 1", List.of(action1));
    Link link2 = new Link("Travel", "City 2", List.of(action2));

    // Create a passage with links
    List<Link> links = new ArrayList<>();
    links.add(link1);
    links.add(link2);
    Passage passage = new Passage("Title", "Content", links);

    // Verify the links and actions
    assertEquals(2, passage.getLinks().size());
    assertTrue(passage.getLinks().contains(link1));
    assertTrue(passage.getLinks().contains(link2));
    assertEquals(1, link1.getActions().size());
    assertEquals(1, link2.getActions().size());
    assertTrue(link1.getActions().contains(action1));
    assertTrue(link2.getActions().contains(action2));
  }

  @Test
  @DisplayName("Test Passage Equality")
  public void testPassageEquality() {
    Link link = new Link("Travel", "City", new ArrayList<>());
    ArrayList<Link> links = new ArrayList<>();
    links.add(link);
    // Create a passage
    Passage passage1 = new Passage("Title", "Content", links);

    // Create an identical passage
    Passage passage2 = new Passage("Title", "Content", links);

    // Create a passage with different title
    Passage passage3 = new Passage("Different Title", "Content", links);

    // Create a passage with different content
    Passage passage4 = new Passage("Title", "Different Content", new ArrayList<>());
    passage4.addLink(link);

    // Verify passage equality
    assertEquals(passage1, passage2);
    assertNotEquals(passage1, passage3);
    assertTrue(passage4.hasLinks());
    assertNotEquals(passage1, passage4);
  }

  @Test
  @DisplayName("Test Passage Hash Code")
  public void testPassageHashCode() {
    // Create a passage
    Passage passage1 = new Passage("Title", "Content", new ArrayList<>());

    // Create an identical passage
    Passage passage2 = new Passage("Title", "Content", new ArrayList<>());

    // Verify passage hash code
    assertEquals(passage1.hashCode(), passage2.hashCode());
  }


}
