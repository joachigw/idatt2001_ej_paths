package edu.ntnu.idatt2001.mappevurdering.ej;

import edu.ntnu.idatt2001.mappevurdering.ej.filehandling.ReadFile;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.Action;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.HealthAction;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.Goal;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.GoldGoal;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.HealthGoal;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.InventoryGoal;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.ScoreGoal;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Game;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Link;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Passage;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Story;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Class for creating test objects.
 */
public class CreateTestObjects {

  private final ReadFile reader = new ReadFile();

  private final File selectedScript =
          new File("src/main/resources/0_Scripts/thevalleyofadventures.paths");

  private final HealthAction action = new HealthAction(-40);
  private final List<Action> actions = new ArrayList<>();
  private Game selectedGame;

  /**Creates a test game.
   *
   * @throws IOException if the file is not found
   */
  public void createTest() throws IOException {



    Map<Link, Passage> testPassageList = new HashMap<>();
    actions.add(action);
    Link openingLink = new Link("Opening Link", "Opening Passage", actions);
    Passage currentPassage = reader.readPassage(openingLink.getReference(), selectedScript);
    testPassageList.put(openingLink, currentPassage);



    ArrayList<Goal> goalList = new ArrayList<>();
    Goal goldGoal = new GoldGoal(100);
    Goal scoreGoal = new ScoreGoal(1000);
    Goal healthGoal = new HealthGoal(0);
    Goal invenGoal = new InventoryGoal("Sword");

    goalList.add(goldGoal);
    goalList.add(scoreGoal);
    goalList.add(healthGoal);
    goalList.add(invenGoal);

    Player player = new Player.Builder("test").gold(10).health(100).score(0).build();
    String inputStoryTitle = "test";
    Story selectedStory = new Story(inputStoryTitle, testPassageList, selectedScript);
    selectedGame = new Game("test", player, selectedStory, goalList, currentPassage, openingLink);
  }

  public Game createTestGame() throws IOException {
    createTest();
    return selectedGame;
  }
}
