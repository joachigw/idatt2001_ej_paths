package edu.ntnu.idatt2001.mappevurdering.ej.modelTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.Action;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.GoldAction;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.HealthAction;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.Goal;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.GoldGoal;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Game;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Link;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Passage;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Story;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Test class for the Game class.
 */
public class GameTest {

  @Test
  @DisplayName("Test Game Creation and Getters")
  public void testGameCreation() {
    // Create a test player
    Player player = new Player.Builder("Test Player")
        .gold(100)
        .health(100)
        .score(0)
        .build();

    // Create a test story
    Map<Link, Passage> passages = new HashMap<>();
    Story story = new Story("Test Story", passages, new File("script.txt"));

    // Create a test list of goals
    List<Goal> goals = new ArrayList<>();

    // Create a test passage
    Passage passage = new Passage("Test Passage", "Test Content", null);

    // Create a test link
    Link link = new Link("Test Link", "Test Reference", new ArrayList<>());

    // Create a test game
    Game game = new Game("Test Game", player, story, goals, passage, link);

    // Test game creation
    assertNotNull(game);
    assertEquals("Test Game", game.getTitle());
    assertEquals(player, game.getPlayer());
    assertEquals(story, game.getStory());
    assertEquals(goals, game.getGoals());
    assertEquals(passage, game.getCurrentPassage());
    assertEquals(link, game.getCurrentLink());
  }


  @Test
  @DisplayName("Test Game Restart")
  public void testGameRestart() {
    // Create a test player
    Player player = new Player.Builder("Test Player")
        .gold(100)
        .health(100)
        .score(0)
        .build();




    // Create a test passage
    List<Link> links = new ArrayList<>();
    links.add(new Link("Test1", "Content", null));




    // Modify the game state
    player.addGold(50);
    player.addHealth(50);
    player.addScore(10);
    // Create a test story
    Map<Link, Passage> passages = new HashMap<>();
    Story story = new Story("Test Story", passages, new File("script.txt"));
    Passage passage = new Passage("Test Passage", "Test Content", links);
    // Create a test link
    Link link = new Link("Test Link", "Test Reference", new ArrayList<>());
    passages.put(link, passage);

    // Create a test game
    // Create a test list of goals
    List<Goal> goals = new ArrayList<>();
    Game game = new Game("Test Game", player, story, goals, passage, link);
    game.getGoals().add(new GoldGoal(500));

    Passage modifiedPassage = new Passage("Modified Passage", "Modified Content", null);
    ArrayList<Action> actions = new ArrayList<>();
    actions.add(new HealthAction(100));
    actions.add(new GoldAction(40));
    Link modifiedLink = new Link("Modified Link", "Modified Reference", actions);

    game.setCurrentPassage(modifiedPassage);
    game.setCurrentLink(modifiedLink);

    // Restart the game
    game.restartGame();

    // Verify the player has been reset
    assertEquals("Test Player", game.getPlayer().getName());
    assertEquals(150, game.getPlayer().getGold());
    assertEquals(150, game.getPlayer().getHealth());
    assertEquals(10, game.getPlayer().getScore());

    // Verify the story has been reset
    assertEquals("Test Story", game.getStory().getTitle());
    assertEquals(1, game.getStory().getPassages().size());
    assertEquals(new File("script.txt"), game.getStory().getScript());

    // Verify the current passage and link have been reset
    assertEquals("Test Passage", game.getCurrentPassage().getTitle());
    assertEquals("Test Content", game.getCurrentPassage().getContent());
    assertEquals(1, game.getCurrentPassage().getLinks().size());
    assertEquals("Test Link", game.getCurrentLink().getText());
    assertEquals("Test Reference", game.getCurrentLink().getReference());
    assertEquals(0, game.getCurrentLink().getActions().size());
  }

}