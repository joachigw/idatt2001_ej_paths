package edu.ntnu.idatt2001.mappevurdering.ej.interfaceTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.action.HealthAction;
import edu.ntnu.idatt2001.mappevurdering.ej.interfaces.goal.HealthGoal;
import edu.ntnu.idatt2001.mappevurdering.ej.models.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Test class for the HealthAction and HealthGoal classes.
 */
public class HealthTest {
  @Test
  @DisplayName("Health Action Test")
  public void testHealthAction() {
    // Create a player with initial health of 100
    Player player = new Player.Builder("Test Player").health(100).build();

    // Create a HealthAction with a positive value
    HealthAction healthAction = new HealthAction(50);
    // Execute the action on the player
    healthAction.execute(player);
    // Check if the player's health has increased from 100 to 150
    assertEquals(150, player.getHealth());

    // Create a HealthAction with a negative value
    healthAction = new HealthAction(-20);
    // Execute the action on the player
    healthAction.execute(player);
    // Check if the player's health has decreased from 150 to 130
    assertEquals(130, player.getHealth());
  }

  @Test
  @DisplayName("Health Action toString Test")
  public void testActionToString() {
    // Create a HealthAction with a negative health value
    HealthAction healthAction = new HealthAction(-50);

    // Get the string representation of the action
    String actionString = healthAction.toString();

    // Check if the string representation is correct
    assertEquals("You lost: 50 health...", actionString);
  }

  @Test
  @DisplayName("Health Goal is Fulfilled")
  public void testHealthIsFulfilled() {
    // Create a player with initial health of 150
    Player player = new Player.Builder("Test Player").health(150).build();

    // Create a HealthGoal with a health goal of 100
    HealthGoal healthGoal = new HealthGoal(100);

    // Check if the health goal is fulfilled for the player
    assertTrue(healthGoal.isFulfilled(player));
  }

  @Test
  @DisplayName("Health Goal toString Test")
  public void testGoalToString() {
    // Create a HealthGoal with a health goal of 200
    HealthGoal healthGoal = new HealthGoal(200);

    // Get the string representation of the goal
    String goalString = healthGoal.toString();

    // Check if the string representation is correct
    assertEquals("Health Goal: reach '200' health.", goalString);
  }

}
